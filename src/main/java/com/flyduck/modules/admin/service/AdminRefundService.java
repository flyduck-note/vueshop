package com.flyduck.modules.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.admin.vo.*;

import java.util.List;

/**
 * <p>
 * AdminRefundService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-13
 */
public interface AdminRefundService {
    List<AdminDictRespVO> getRefundStatusList();

    Page<AdminRefundRespVO> getRefundPage(Page page, AdminRefundQueryReqVO adminRefundQueryReqVO);

    AdminRefundDetailsRespVO getRefundDetailsById(Long id);

    void auditRefund(AdminRefundAuditReqVO adminRefundAuditReqVO);
}
