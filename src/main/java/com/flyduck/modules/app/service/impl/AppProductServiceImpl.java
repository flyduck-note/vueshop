package com.flyduck.modules.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.exception.BusinessException;
import com.flyduck.entity.AppProduct;
import com.flyduck.entity.AppSkuStock;
import com.flyduck.entity.AppSpecificationValue;
import com.flyduck.mapper.*;
import com.flyduck.manager.AppSkuStockManager;
import com.flyduck.modules.app.constant.AppConstant;
import com.flyduck.modules.app.vo.product.AppProductQueryReqVO;
import com.flyduck.modules.app.vo.product.AppProductRespVO;
import com.flyduck.modules.app.vo.product.AppProductDetailsRespVO;
import com.flyduck.manager.AppSpecificationManager;
import com.flyduck.modules.app.service.AppProductService;
import com.flyduck.utils.BeanUtils;
import com.flyduck.utils.PageUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 21:51
 **/
@Service
public class AppProductServiceImpl implements AppProductService {

    @Resource
    private AppCategoryMapper appCategoryMapper;
    @Resource
    private AppProductMapper appProductMapper;
    @Resource
    private AppSpecificationManager appSpecificationManager;
    @Resource
    private AppSkuStockManager appSkuStockManager;

    @Override
    public Page<AppProductRespVO> getProductPage(Page page, AppProductQueryReqVO appProductQueryReqVO) {
        Page productPage = appProductMapper.selectPage(page,
                new LambdaQueryWrapper<AppProduct>()
                        .eq(AppProduct::getIsOnSale, true)
                        .eq(AppProduct::getCategoryId, appProductQueryReqVO.getCategoryId())
                        .orderByDesc(AppConstant.AppProductSort.DEFAULT_SORT.getCode() == appProductQueryReqVO.getSort(), AppProduct::getIsTop, AppProduct::getSortOrder)//0:默认排序
                        .orderByDesc(AppConstant.AppProductSort.NEWEST_FIRST.getCode() == appProductQueryReqVO.getSort(), AppProduct::getCreated)//1:按照上市时间
                        .orderByDesc(AppConstant.AppProductSort.HIGHEST_SALES.getCode() == appProductQueryReqVO.getSort(), AppProduct::getSale)//2:销量
        );

        return PageUtils.convert(productPage,AppProductRespVO.class);
    }

    @Override
    public AppProductDetailsRespVO getProductDetailsById(Long productId) {
        // 商品详情
        AppProduct appProduct = appProductMapper.selectById(productId);
        if (appProduct == null) {
            throw new BusinessException("商品不存在");
        }
        if (!appProduct.getIsOnSale()) {
            throw new BusinessException("商品未上架");
        }

        AppProductDetailsRespVO appProductDetailsRespVO = BeanUtils.toBean(appProduct, AppProductDetailsRespVO.class);

        // 商品属性
        List<AppSpecificationValue> specificationValueList = appSpecificationManager.getAppSpecificationValueListByProductId(productId);
        List<AppProductDetailsRespVO.AppSpecificationValueVO> appSpecificationValueVOS = BeanUtils.toBeanList(specificationValueList, AppProductDetailsRespVO.AppSpecificationValueVO.class);
        appProductDetailsRespVO.setSpecifications(appSpecificationValueVOS);

        // 商品可选sku及库存
        List<AppSkuStock> appSkuStockList = appSkuStockManager.getAppSkuStockListByProductId(productId);
        List<AppProductDetailsRespVO.AppSkuStockVO> appSkuStockVOS = BeanUtils.toBeanList(appSkuStockList, AppProductDetailsRespVO.AppSkuStockVO.class);
        appProductDetailsRespVO.setSkuStocks(appSkuStockVOS);

        return appProductDetailsRespVO;
    }
}
