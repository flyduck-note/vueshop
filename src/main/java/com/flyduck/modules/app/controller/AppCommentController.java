package com.flyduck.modules.app.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.annotation.Login;
import com.flyduck.modules.app.service.AppCommentService;
import com.flyduck.modules.app.vo.comment.AppCommentDetailsRespVO;
import com.flyduck.modules.app.vo.comment.AppCommentProductRespVO;
import com.flyduck.modules.app.vo.comment.AppCommentRespVO;
import com.flyduck.modules.app.vo.comment.AppCommentPostReqVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-23 21:37
 **/
@RestController
@RequestMapping("/app/comment")
public class AppCommentController extends BaseController{

    @Resource
    private AppCommentService appCommentService;

    @Login
    @GetMapping("/getCommentPageByProductId/{productId}")
    public Result<Page<AppCommentRespVO>> getCommentPageByProductId(@PathVariable("productId") Long productId){
        Page<AppCommentRespVO> appCommentRespVOPage = appCommentService.getCommentPageByProductId(getPage(),productId);
        return Result.success(appCommentRespVOPage);
    }

    @Login
    @GetMapping("/getCommentDetailsById/{id}")
    public Result<AppCommentDetailsRespVO> getCommentDetailsById(@PathVariable("id") Long id){
        AppCommentDetailsRespVO appCommentDetailsRespVO = appCommentService.getCommentDetailsById(id);
        return Result.success(appCommentDetailsRespVO);
    }

    @Login
    @GetMapping("/getCommentProductPageByStatus")
    public Result<Page<AppCommentProductRespVO>> getCommentProductPageByStatus(Integer status){
        Page<AppCommentProductRespVO> appCommentProductRespVOPage = appCommentService.getCommentProductPageByStatus(getPage(),status);
        return Result.success(appCommentProductRespVOPage);
    }

    @Login
    @PostMapping("/postComment")
    public Result<Long> postComment(MultipartFile[] pics, @Validated AppCommentPostReqVO appCommentPostReqVO){
        Long id = appCommentService.postComment(pics, appCommentPostReqVO);
        return Result.success(id);
    }
}
