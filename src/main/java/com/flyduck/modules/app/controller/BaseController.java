package com.flyduck.modules.app.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.ServletRequestUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 21:45
 **/
public class BaseController {

    @Resource
    private HttpServletRequest request;

    public Page getPage(){
        int current = ServletRequestUtils.getIntParameter(request, "current",1);
        int size = ServletRequestUtils.getIntParameter(request, "size",10);
        return new Page(current, size);
    }
}
