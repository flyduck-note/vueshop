package com.flyduck.modules.app.controller;

import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.annotation.Login;
import com.flyduck.modules.app.service.AppRefundService;
import com.flyduck.modules.app.vo.refund.AppRefundApplyReqVO;
import com.flyduck.modules.app.vo.refund.AppRefundBaseDataRespVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * <p>
 * AppRefundController
 * </p>
 *
 * @author flyduck
 * @since 2024-05-23
 */
@RestController
@RequestMapping("/app/refund")
public class AppRefundController {

    @Resource
    private AppRefundService appRefundService;

    /**
     * 退款界面的基础数据
     */
    @Login
    @GetMapping("/getRefundBaseDataByOrderId/{orderId}")
    public Result<AppRefundBaseDataRespVO> getRefundBaseDataByOrderId(@PathVariable("orderId") Long orderId){
        AppRefundBaseDataRespVO appRefundBaseDataRespVO = appRefundService.getRefundBaseDataByOrderId(orderId);
        return Result.success(appRefundBaseDataRespVO);
    }

    @Login
    @PostMapping("/refundApply")
    public Result<Void> refundApply(MultipartFile[] pics, @Validated AppRefundApplyReqVO appRefundApplyReqVO){
        appRefundService.refundApply(appRefundApplyReqVO,pics);
        return Result.success();
    }
}
