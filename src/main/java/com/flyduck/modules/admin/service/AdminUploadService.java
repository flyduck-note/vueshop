package com.flyduck.modules.admin.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * AdminUploadService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-27
 */
public interface AdminUploadService {
    List<String> upload(MultipartFile[] pics) throws IOException;
}
