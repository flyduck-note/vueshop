package com.flyduck.modules.system.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * SysRoleReqVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysRoleUpdateReqVO implements Serializable {
    /**
     *
     */
    @NotNull(message = "id不能为空")
    private Long id;

    /**
     *
     */
    @NotBlank(message = "角色名称不能为空")
    private String name;

    /**
     *
     */
    @NotBlank(message = "角色编码不能为空")
    private String code;

    /**
     * 备注
     */
    private String remark;
}
