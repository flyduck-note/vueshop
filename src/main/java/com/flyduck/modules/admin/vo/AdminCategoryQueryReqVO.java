package com.flyduck.modules.admin.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:48
 **/
@Data
public class AdminCategoryQueryReqVO implements Serializable {
    private String name;
}
