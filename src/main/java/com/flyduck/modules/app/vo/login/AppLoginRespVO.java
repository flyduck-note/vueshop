package com.flyduck.modules.app.vo.login;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.flyduck.entity.AppUser;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * LoginResponse
 * </p>
 *
 * @author flyduck
 * @since 2024/5/9
 */
@Data
public class AppLoginRespVO implements Serializable {

    /**
     * 令牌
     */
    private String token;
    /**
     * 用户信息
     */
    private AppUserVO userInfo;

    /**
     * 用户信息
     */
    @Data
    public static class AppUserVO extends AppUser implements Serializable{
        @JsonIgnore
        private String password;
    }
}
