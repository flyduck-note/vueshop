package com.flyduck.modules.app.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.annotation.Login;
import com.flyduck.modules.app.annotation.NoRepeatSubmit;
import com.flyduck.modules.app.service.AppOrderService;
import com.flyduck.modules.app.vo.delivery.AppDeliveryInfoRespVO;
import com.flyduck.modules.app.vo.order.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-20 20:40
 **/
@RestController
@RequestMapping("/app/order")
public class AppOrderController extends BaseController{

    @Resource
    private AppOrderService appOrderService;

    @Login
    @PostMapping("previewOrder")
    public Result<AppOrderPreviewRespVO> previewOrder(@RequestBody AppOrderPreviewReqVO appOrderPreviewReqVO){
        AppOrderPreviewRespVO appOrderPreviewRespVO = appOrderService.previewOrder(appOrderPreviewReqVO);
        return Result.success(appOrderPreviewRespVO);
    }

    @Login
    @NoRepeatSubmit
    @PostMapping("createOrder")
    public Result<String> createOrder(@RequestBody AppOrderCreateReqVO appOrderCreateReqVO){
        String orderSn = appOrderService.createOrder(appOrderCreateReqVO);
        return Result.success(orderSn);
    }

    @Login
    @GetMapping("/getOrderTotalCount")
    public Result<AppOrderTotalCountRespVO> getOrderTotalCount(){
        AppOrderTotalCountRespVO appOrderTotalCountRespVO = appOrderService.getOrderTotalCount();
        return Result.success(appOrderTotalCountRespVO);
    }

    @Login
    @GetMapping("/getOrderPageByStatus")
    public Result<Page<AppOrderRespVO>> getOrderPageByStatus(Integer status){
        return Result.success(appOrderService.getOrderPageByStatus(getPage(),status));
    }

    @Login
    @GetMapping("/getOrderDetailsById/{id}")
    public Result<AppOrderDetailsRespVO> getOrderDetailsById(@PathVariable("id") Long id){
        return Result.success(appOrderService.getOrderDetailsById(id));
    }

    @Login
    @PostMapping("/cancelOrderById/{id}")
    public Result<Void> cancelOrderById(@PathVariable("id")Long id){
        appOrderService.cancelOrderById(id);
        return Result.success();
    }

    @Login
    @PostMapping("/deleteById/{id}")
    public Result<Void> deleteById(@PathVariable("id")Long id){
        appOrderService.deleteById(id);
        return Result.success();
    }

    @Login
    @GetMapping("/getDeliveryInfoById/{id}")
    public Result<AppDeliveryInfoRespVO> getDeliveryInfoById(@PathVariable("id")Long id) throws Exception {
        return Result.success(appOrderService.getDeliveryInfoById(id));
    }

    @Login
    @PostMapping("/confirmOrderById/{id}")
    public Result<Void> confirmOrderById(@PathVariable("id")Long id){
        appOrderService.confirmOrderById(id);
        return Result.success();
    }
}
