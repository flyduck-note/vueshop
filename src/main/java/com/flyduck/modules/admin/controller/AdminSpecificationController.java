package com.flyduck.modules.admin.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.admin.service.AdminSpecificationService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.modules.app.controller.BaseController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Encoder;
import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import cn.hutool.crypto.digest.DigestAlgorithm;

/**
 * <p>
 * SysMenuController
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@RestController
@RequestMapping("/admin/specification")
public class AdminSpecificationController extends BaseController {

    @Resource
    private AdminSpecificationService sysSpecificationService;

    @GetMapping("/getSpecificationPage")
    public Result<Page<AdminSpecificationRespVO>> getSpecificationPage(AdminSpecificationQueryReqVO sysSpecificationQueryReqVO){
        Page<AdminSpecificationRespVO> adminSpecificationRespVOPage = sysSpecificationService.getSpecificationPage(getPage(),sysSpecificationQueryReqVO);
        return Result.success(adminSpecificationRespVOPage);
    }

    @GetMapping("/getSpecificationDetailsById/{id}")
    public Result<AdminSpecificationDetailsRespVO> getSpecificationDetailsById(@PathVariable("id") Long id){
        AdminSpecificationDetailsRespVO sysSpecificationDetailsRespVO = sysSpecificationService.getSpecificationDetailsById(id);
        return Result.success(sysSpecificationDetailsRespVO);
    }

    @PostMapping("/saveSpecification")
    public Result<Void> saveSpecification(@Validated @RequestBody AdminSpecificationSaveReqVO adminSpecificationSaveReqVO){
        sysSpecificationService.saveSpecification(adminSpecificationSaveReqVO);
        return Result.success();
    }

    @PostMapping("/updateSpecification")
    public Result<Void> updateSpecification(@Validated @RequestBody AdminSpecificationUpdateReqVO adminSpecificationUpdateReqVO){
        sysSpecificationService.updateSpecification(adminSpecificationUpdateReqVO);
        return Result.success();
    }

    @PostMapping("/batchDeleteByIds")
    public Result<Void> batchDeleteByIds(@RequestBody Long[] ids){
        sysSpecificationService.batchDeleteByIds(Arrays.asList(ids));
        return Result.success();
    }


}
