package com.flyduck.modules.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.controller.BaseController;
import com.flyduck.modules.system.service.SysRoleService;
import com.flyduck.modules.system.vo.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * <p>
 * SysMenuController
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends BaseController {

    @Resource
    private SysRoleService sysRoleService;

    @GetMapping("/getRolePage")
    @PreAuthorize("hasAuthority('sys:role:list')")
    public Result<Page<SysRoleRespVO>> getRolePage(SysRoleQueryReqVO sysRoleQueryReqVO){
        Page<SysRoleRespVO> sysRoleRespVOPage = sysRoleService.getRolePage(getPage(),sysRoleQueryReqVO);
        return Result.success(sysRoleRespVOPage);
    }

    @GetMapping("/getRoleDetailsById/{id}")
    @PreAuthorize("hasAuthority('sys:role:list')")
    public Result<SysRoleDetailsRespVO> getRoleDetailsById(@PathVariable("id") Long id){
        SysRoleDetailsRespVO sysRoleDetailsRespVO = sysRoleService.getRoleDetailsById(id);
        return Result.success(sysRoleDetailsRespVO);
    }

    @PostMapping("/saveRole")
    @PreAuthorize("hasAuthority('sys:role:save')")
    public Result<Void> saveRole(@Validated @RequestBody SysRoleSaveReqVO sysRoleSaveReqVO){
        sysRoleService.saveRole(sysRoleSaveReqVO);
        return Result.success();
    }

    @PostMapping("/updateRole")
    @PreAuthorize("hasAuthority('sys:role:update')")
    public Result<Void> updateRole(@Validated @RequestBody SysRoleUpdateReqVO sysRoleUpdateReqVO){
        sysRoleService.updateRole(sysRoleUpdateReqVO);
        return Result.success();
    }

    @PostMapping("/batchDeleteByIds")
    @PreAuthorize("hasAuthority('sys:role:delete')")
    public Result<Void> batchDeleteByIds(@RequestBody Long[] ids){
        sysRoleService.batchDeleteByIds(Arrays.asList(ids));
        return Result.success();
    }

    @PostMapping("/assignMenusToRole/{roleId}")
    @PreAuthorize("hasAuthority('sys:role:perm')")
    public Result<Void> assignMenusToRole(@PathVariable(name = "roleId")Long roleId,@RequestBody Long[] menuIds){
        sysRoleService.assignMenuListToRole(roleId,Arrays.asList(menuIds));
        return Result.success();
    }

}
