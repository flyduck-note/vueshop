package com.flyduck.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 
 * @TableName app_product
 */
@TableName(value ="app_product")
@Data
public class AppProduct implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 
     */
    private String sn;

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    private Long categoryId;

    /**
     * 
     */
    private String image;

    /**
     * 
     */
    private String pics;

    /**
     * 
     */
    private BigDecimal price;

    /**
     * 
     */
    private BigDecimal cost;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 
     */
    private String keywords;

    /**
     * 
     */
    private String detail;

    /**
     * 
     */
    private Boolean isOnSale;

    /**
     * 
     */
    private Boolean isNew;

    /**
     * 
     */
    private Boolean isTop;

    /**
     * 
     */
    private Boolean isHot;

    /**
     * 
     */
    private Integer sortOrder;

    /**
     * 
     */
    private LocalDateTime created;

    /**
     * 
     */
    private LocalDateTime updated;

    /**
     * 
     */
    private Boolean isDelete;

    /**
     * 
     */
    private Integer sale;

    /**
     * 
     */
    private Integer comments;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}