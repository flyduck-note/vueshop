package com.flyduck.modules.app.service;


import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * <p>
 * AppPayService
 * </p>
 *
 * @author flyduck
 * @since 2024-05-22
 */
public interface AppPayService {

    String orderPayByOrderSn(String orderSn);

    void payNotify(Map<String, String> paramMap, HttpServletResponse response) throws IOException;

    String payCheckByOrderSn(String orderSn);
}
