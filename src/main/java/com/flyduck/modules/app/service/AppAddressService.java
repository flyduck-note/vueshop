package com.flyduck.modules.app.service;

import com.flyduck.modules.app.vo.address.AppAddressDetailsRespVO;
import com.flyduck.modules.app.vo.address.AppAddressRespVO;
import com.flyduck.modules.app.vo.address.AppAddressSaveReqVO;
import com.flyduck.modules.app.vo.address.AppAddressUpdateReqVO;

import java.util.List;

/**
 * <p>
 * AppAddressService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-19
 */
public interface AppAddressService {
    List<AppAddressRespVO> getAddressList();

    AppAddressDetailsRespVO getAddressDetailsById(Long id);

    void saveAddress(AppAddressSaveReqVO appAddressSaveReqVO);

    void updateAddress(AppAddressUpdateReqVO appAddressUpdateReqVO);

    void deleteById(Long id);
}
