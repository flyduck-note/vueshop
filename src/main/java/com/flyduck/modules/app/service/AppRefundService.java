package com.flyduck.modules.app.service;

import com.flyduck.modules.app.vo.refund.AppRefundApplyReqVO;
import com.flyduck.modules.app.vo.refund.AppRefundBaseDataRespVO;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * AppRefundService
 * </p>
 *
 * @author flyduck
 * @since 2024-05-23
 */
public interface AppRefundService {
    AppRefundBaseDataRespVO getRefundBaseDataByOrderId(Long orderId);

    void refundApply(AppRefundApplyReqVO appRefundApplyReqVO, MultipartFile[] pics);

}
