package com.flyduck.modules.admin.vo;

import com.flyduck.entity.AppProduct;
import com.flyduck.entity.AppSkuStock;
import com.flyduck.entity.AppSpecification;
import com.flyduck.entity.AppSpecificationValue;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * AdminSkuStockRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-06
 */
@Data
public class AdminSkuDetailsRespVO implements Serializable {

    private AppProductVO product;
    private List<AppSpecificationVO> specs;
    private List<AppSpecificationValueVO> specValues;
    private List<String> selectSpecValues;
    private List<AppSkuStockVO> skuStocks;

    @Data
    public static class AppProductVO extends AppProduct implements Serializable{

    }

    @Data
    public static class AppSpecificationVO extends AppSpecification implements Serializable{

    }

    @Data
    public static class AppSpecificationValueVO extends AppSpecificationValue implements Serializable{

    }

    @Data
    public static class AppSkuStockVO extends AppSkuStock implements Serializable{

    }
}
