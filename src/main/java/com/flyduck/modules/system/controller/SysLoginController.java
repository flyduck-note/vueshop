package com.flyduck.modules.system.controller;

import com.flyduck.common.lang.Result;
import com.flyduck.modules.system.service.SysLoginService;
import com.flyduck.modules.system.vo.SysUserInfoRespVO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-25 08:29
 **/
@RestController
@RequestMapping("/sys")
public class SysLoginController {
    @Resource
    private SysLoginService sysLoginService;

    @GetMapping("/getUserInfo")
    public Result<SysUserInfoRespVO> getUserInfo(){
        SysUserInfoRespVO sysUserInfoRespVO = sysLoginService.getUserInfo();
        return Result.success(sysUserInfoRespVO);
    }

}
