package com.flyduck.modules.admin.vo;

import com.flyduck.entity.AppSkuStock;
import com.flyduck.entity.AppSpecificationValue;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * AdminSkuSaveReqVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-11
 */
@Data
public class AdminSkuSaveReqVO implements Serializable {

    @NotNull(message = "商品id不能为空")
    private Long productId;

    private List<AppSpecificationValueVO> specValues;

    private List<AppSkuStockVO> skuStocks;

    @Data
    public static class AppSpecificationValueVO extends AppSpecificationValue implements Serializable{

    }

    @Data
    public static class AppSkuStockVO extends AppSkuStock implements Serializable{

    }


}
