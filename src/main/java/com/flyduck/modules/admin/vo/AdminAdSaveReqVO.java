package com.flyduck.modules.admin.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:48
 **/
@Data
public class AdminAdSaveReqVO implements Serializable {
    /**
     *
     */
    @NotBlank(message = "广告名称不能为空")
    private String name;

    /**
     *
     */
    @NotBlank(message = "广告位置不能为空")
    private String position;

    /**
     *
     */
    @NotBlank(message = "广告照片不能为空")
    private String image;

    /**
     *
     */
    @NotBlank(message = "广告ur不能为空")
    private String url;

    /**
     *
     */
    @NotNull(message = "状态不能为空")
    private Boolean isShow;


    /**
     *
     */
    @NotNull(message = "排序不能为空")
    private Integer sortOrder;
}
