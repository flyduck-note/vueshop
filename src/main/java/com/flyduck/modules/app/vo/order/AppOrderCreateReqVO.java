package com.flyduck.modules.app.vo.order;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-20 20:44
 **/
@Data
public class AppOrderCreateReqVO implements Serializable {

    @NotNull(message = "请选择地址")
    private Long addressId;
    private List<Long> cartIds;
    private String note;

    private Long productId;
    private Long skuId;
    private Integer quantity;
}
