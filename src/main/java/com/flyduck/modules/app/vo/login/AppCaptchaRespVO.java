package com.flyduck.modules.app.vo.login;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * LoginRequest
 * </p>
 *
 * @author flyduck
 * @since 2024/5/9
 */
@Data
public class AppCaptchaRespVO implements Serializable {

    private String uuid;

    private String base64Img;
}
