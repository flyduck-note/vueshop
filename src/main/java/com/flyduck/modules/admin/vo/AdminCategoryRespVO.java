package com.flyduck.modules.admin.vo;

import com.flyduck.entity.AppCategory;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:48
 **/
@Data
public class AdminCategoryRespVO extends AppCategory implements Serializable {
}
