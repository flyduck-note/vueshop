package com.flyduck.modules.system.controller;

import com.flyduck.common.lang.Result;
import com.flyduck.modules.system.service.SysMenuService;
import com.flyduck.modules.system.vo.SysMenuDetailsRespVO;
import com.flyduck.modules.system.vo.SysMenuSaveReqVO;
import com.flyduck.modules.system.vo.SysMenuTreeRespVO;
import com.flyduck.modules.system.vo.SysMenuUpdateReqVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * SysMenuController
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController {

    @Resource
    private SysMenuService sysMenuService;

    @GetMapping("/getMenuTree")
    @PreAuthorize("hasAuthority('sys:menu:list')")
    public Result<List<SysMenuTreeRespVO>> getMenuTree(){
        List<SysMenuTreeRespVO> sysMenuTreeRespVOList = sysMenuService.getMenuTree();
        return Result.success(sysMenuTreeRespVOList);
    }

    @GetMapping("/getMenuDetailsById/{id}")
    @PreAuthorize("hasAuthority('sys:menu:list')")
    public Result<SysMenuDetailsRespVO> getMenuDetailsById(@PathVariable("id") Long id){
        SysMenuDetailsRespVO sysMenuDetailsRespVO = sysMenuService.getMenuDetailsById(id);
        return Result.success(sysMenuDetailsRespVO);
    }

    @PostMapping("/saveMenu")
    @PreAuthorize("hasAuthority('sys:menu:save')")
    public Result<Void> saveMenu(@Validated @RequestBody SysMenuSaveReqVO sysMenuSaveReqVO){
        sysMenuService.saveMenu(sysMenuSaveReqVO);
        return Result.success();
    }

    @PostMapping("/updateMenu")
    @PreAuthorize("hasAuthority('sys:menu:update')")
    public Result<Void> updateMenu(@Validated @RequestBody SysMenuUpdateReqVO sysMenuUpdateReqVO){
        sysMenuService.updateMenu(sysMenuUpdateReqVO);
        return Result.success();
    }

    @PostMapping("/deleteById/{id}")
    @PreAuthorize("hasAuthority('sys:menu:delete')")
    public Result<Void> deleteById(@PathVariable("id") Long id){
        sysMenuService.deleteById(id);
        return Result.success();
    }

}
