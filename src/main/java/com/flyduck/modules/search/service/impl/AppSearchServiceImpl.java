package com.flyduck.modules.search.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.search.document.EsProductDoc;
import com.flyduck.modules.search.service.AppSearchService;
import com.flyduck.modules.search.vo.AppEsProductRespVO;
import com.flyduck.utils.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * AppSearchServiceImpl
 * </p>
 *
 * @author flyduck
 * @since 2024-05-24
 */
@Service
public class AppSearchServiceImpl implements AppSearchService {

    @Resource
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Override
    public Page<AppEsProductRespVO> getEsProductPage(String keyword, int current, int size) {

        Criteria criteria = new Criteria("name").matches(keyword)
                .or(
                        new Criteria("keyword").matches(keyword)
                );

        //springDataJpa的分页从0开始，所以current-1
        Query query = new CriteriaQuery(criteria).setPageable(PageRequest.of(current - 1, size));

        SearchHits<EsProductDoc> searchHits = elasticsearchRestTemplate.search(query, EsProductDoc.class);
        List<EsProductDoc> esProductDocList = searchHits.get().map(SearchHit::getContent).collect(Collectors.toList());
        List<AppEsProductRespVO> appEsProductRespVOList = BeanUtils.toBeanList(esProductDocList, AppEsProductRespVO.class);

        Page<AppEsProductRespVO> page = new Page<>(current,size,searchHits.getTotalHits());
        page.setRecords(appEsProductRespVOList);
        return page;
    }
}
