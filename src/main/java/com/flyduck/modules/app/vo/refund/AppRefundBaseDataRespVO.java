package com.flyduck.modules.app.vo.refund;

import com.flyduck.entity.AppOrder;
import com.flyduck.entity.AppOrderItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * AppRefundBaseDataRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-05-23
 */
@Data
public class AppRefundBaseDataRespVO implements Serializable {

    private AppOrderDTO order;
    //退货原因
    private List<String> reasons;
    //退货方式
    private List<String> methods;

    @Data
    public static class AppOrderDTO extends AppOrder implements Serializable{
        private List<AppOrderItemDTO> orderItems;
    }

    @Data
    public static class AppOrderItemDTO extends AppOrderItem implements Serializable{

    }
}
