package com.flyduck.modules.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.admin.vo.*;

import java.util.List;

/**
 * <p>
 * AdminCategoryService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-04
 */
public interface AdminCategoryService {
    Page<AdminCategoryRespVO> getCategoryPage(Page page, AdminCategoryQueryReqVO sysCategoryQueryReqVO);

    AdminCategoryDetailsRespVO getCategoryDetailsById(Long id);

    void saveCategory(AdminCategorySaveReqVO adminCategorySaveReqVO);

    void updateCategory(AdminCategoryUpdateReqVO adminCategoryUpdateReqVO);

    void batchDeleteByIds(List<Long> ids);

    List<AdminCategoryRespVO> getCategoryList();
}
