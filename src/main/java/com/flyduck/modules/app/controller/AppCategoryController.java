package com.flyduck.modules.app.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.service.AppCategoryService;
import com.flyduck.modules.app.service.AppProductService;
import com.flyduck.modules.app.vo.category.AppCategoryDetailsRespVO;
import com.flyduck.modules.app.vo.product.AppProductDetailsRespVO;
import com.flyduck.modules.app.vo.product.AppProductRespVO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 21:42
 **/
@RestController
@RequestMapping("/app/category")
public class AppCategoryController extends BaseController{

    @Resource
    private AppCategoryService appCategoryService;

    @GetMapping("/getCategoryDetailsById/{id}")
    public Result<AppCategoryDetailsRespVO> getCategoryDetailsById(@PathVariable("id") Long id){
        AppCategoryDetailsRespVO appCategoryDetailsRespVO = appCategoryService.getCategoryDetailsById(id);
        return Result.success(appCategoryDetailsRespVO);
    }
}
