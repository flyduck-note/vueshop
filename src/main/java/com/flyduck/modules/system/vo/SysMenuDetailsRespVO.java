package com.flyduck.modules.system.vo;

import com.flyduck.entity.SysMenu;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * SysMenuDetailsVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysMenuDetailsRespVO extends SysMenu implements Serializable {
}
