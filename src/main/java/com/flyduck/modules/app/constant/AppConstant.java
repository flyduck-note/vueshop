package com.flyduck.modules.app.constant;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 10:17
 **/
public class AppConstant {

    public static class RedisKeyConstant {
        public static final String APP_CAPTCHA_PREFIX = "app:captcha:";
    }

    public static final String TOKEN_HEADER_KEY = "token";
    public static final String SESSION_USER_KEY = "userId";

    public static enum AppProductSort {
        DEFAULT_SORT(0, "默认排序"),
        NEWEST_FIRST(1, "最新上市"),
        HIGHEST_SALES(2, "销量最多");

        private int code;
        private String description;

        AppProductSort(int code, String description) {
            this.code = code;
            this.description = description;
        }

        public int getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }
    }

    public static enum AppAdPosition {
        carousel("carousel", "轮播图"),;

        private final String code;
        private final String description;

        AppAdPosition(String code, String description) {
            this.code = code;
            this.description = description;
        }

        public String getCode() {
            return code;
        }
    }

    public static enum RefundStatus {
        PENDING(0, "待处理"),
        PROCESSING(1, "退货中"),
        COMPLETED(2, "已完成"),
        REJECTED(3, "已拒绝");

        private final int code;
        private final String description;

        RefundStatus(int code, String description) {
            this.code = code;
            this.description = description;
        }

        public int getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }

        // 根据代码获取枚举值
        public static RefundStatus fromCode(int code) {
            for (RefundStatus status : values()) {
                if (status.getCode() == code) {
                    return status;
                }
            }
            throw new IllegalArgumentException("Invalid code: " + code);
        }
    }

    public static enum OrderStatus {
        PENDING_PAYMENT(0, "待付款"),
        PENDING_DELIVERY(1, "待发货"),
        PENDING_RECEIPT(2, "待收货"),
        COMPLETED(3, "已完成"),
        CANCELLED(4, "已取消"),
        REFUND_REQUESTED(5, "申请退款"),
        REFUNDED(6, "已退款"),
        REFUND_FAILED(7, "退款失败");

        private final int code;
        private final String description;

        OrderStatus(int code, String description) {
            this.code = code;
            this.description = description;
        }

        public int getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }

        // 根据代码获取枚举值
        public static OrderStatus fromCode(int code) {
            for (OrderStatus status : values()) {
                if (status.getCode() == code) {
                    return status;
                }
            }
            throw new IllegalArgumentException("Invalid code: " + code);
        }
    }

    public static enum ReturnStatus  {
        PENDING_PROCESSING(0, "待处理"),
        IN_PROGRESS(1, "退货中"),
        COMPLETED(2, "已完成"),
        REJECTED(3, "已拒绝");

        private int code;
        private String description;

        ReturnStatus (int code, String description) {
            this.code = code;
            this.description = description;
        }

        public int getCode() {
            return code;
        }
    }

}
