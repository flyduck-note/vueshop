package com.flyduck.modules.admin.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:48
 **/
@Data
public class AdminSpecificationUpdateReqVO implements Serializable {
    /**
     *
     */
    @NotNull(message = "id不能为空")
    private Long id;

    /**
     *
     */
    @NotBlank(message = "规格名称不能为空")
    private String name;

    /**
     * 可选项
     */
    @NotBlank(message = "可选值不能为空")
    private String options;

    /**
     *
     */
    @NotNull(message = "排序不能为空")
    private Integer sortOrder;

}
