package com.flyduck.modules.search.repository;

import com.flyduck.modules.search.document.EsProductDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * <p>
 * EsProductDocRepository
 * </p>
 *
 * @author flyduck
 * @since 2024-05-24
 */
public interface EsProductDocRepository extends ElasticsearchRepository<EsProductDoc,Long> {
}
