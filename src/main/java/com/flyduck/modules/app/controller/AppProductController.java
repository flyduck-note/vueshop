package com.flyduck.modules.app.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.vo.product.AppProductQueryReqVO;
import com.flyduck.modules.app.vo.product.AppProductRespVO;
import com.flyduck.modules.app.vo.product.AppProductDetailsRespVO;
import com.flyduck.modules.app.service.AppProductService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 21:42
 **/
@RestController
@RequestMapping("/app/product")
public class AppProductController extends BaseController{

    @Resource
    private AppProductService appProductService;

    @GetMapping("/getProductPage")
    public Result<Page<AppProductRespVO>> getProductPage(AppProductQueryReqVO appProductQueryReqVO){
        Page<AppProductRespVO> appProductRespVOPage = appProductService.getProductPage(getPage(),appProductQueryReqVO);
        return Result.success(appProductRespVOPage);
    }

    @GetMapping("/getProductDetailsById/{id}")
    public Result<AppProductDetailsRespVO> getProductDetailsById(@PathVariable("id") Long id){
        AppProductDetailsRespVO appProductDetailsRespVO =  appProductService.getProductDetailsById(id);
        return Result.success(appProductDetailsRespVO);
    }
}
