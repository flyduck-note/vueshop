package com.flyduck.modules.search.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.search.vo.AppEsProductRespVO;

/**
 * <p>
 * AppSearchService
 * </p>
 *
 * @author flyduck
 * @since 2024-05-24
 */
public interface AppSearchService {
    Page<AppEsProductRespVO> getEsProductPage(String keyword, int current, int size);
}
