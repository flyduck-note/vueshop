package com.flyduck.modules.system.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * SysUserInfoRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysUserInfoRespVO {

    private SysUserInfoVO userInfo;
    private List<SysMenuVO> menuList;
    private List<String> permissionList;

    @Data
    public static class SysUserInfoVO implements Serializable {

        private Long id;
        private String username;
        private String avatar;
    }

    @Data
    public static class SysMenuVO implements Serializable {

        private Long id;
        private Long parentId;
        private Integer type;

        private String title;
        private String icon;
        private String path;
        private String component;
        private String name;

        private List<SysMenuVO> children = new ArrayList<>();
    }
}
