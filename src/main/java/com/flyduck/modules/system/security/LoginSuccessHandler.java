package com.flyduck.modules.system.security;

import cn.hutool.json.JSONUtil;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.system.vo.SysLoginRespVO;
import com.flyduck.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 * 登录成功处理器
 * </p>
 *
 * @author flyduck
 * @since 2024-05-28
 */
@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private JwtUtils jwtUtils;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        ServletOutputStream outputStream = response.getOutputStream();

        //生成token，返回
        String token = jwtUtils.generateToken(((CustomUser) authentication.getPrincipal()).getUserId(),"ADMIN");
        SysLoginRespVO loginRespVO = new SysLoginRespVO();
        loginRespVO.setToken(token);
        Result<SysLoginRespVO> result = Result.success(loginRespVO);
        outputStream.write(JSONUtil.toJsonStr(result).getBytes("UTF-8"));
        outputStream.flush();
        outputStream.close();
    }
}
