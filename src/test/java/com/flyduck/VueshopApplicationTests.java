package com.flyduck;

import org.aspectj.lang.annotation.After;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class VueshopApplicationTests {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Test
    void contextLoads() {
        System.out.println(passwordEncoder.encode("888888"));
        System.out.println(passwordEncoder.matches("888888","$2a$10$uDh5b9neECPBhgJLpMekSu9wZej2FEhxwbEprMj7WUOK/WiGlXd3S"));
    }

}
