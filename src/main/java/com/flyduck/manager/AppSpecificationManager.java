package com.flyduck.manager;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.entity.AppSpecificationValue;
import com.flyduck.mapper.AppSpecificationValueMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-19 07:42
 **/
@Component
public class AppSpecificationManager {

    @Resource
    private AppSpecificationValueMapper appSpecificationValueMapper;

    /**
     * 根据productId获取商品的规格
     * 如果productId在app_specification_value存在，说明是多规格商品，返回app_specification_value的所有记录
     * 如果productId在app_specification_value不存在，说明是单规格商品，写死一个规格返回，叫做默认规格
     * @param productId
     * @return
     */
    public List<AppSpecificationValue> getAppSpecificationValueListByProductId(Long productId) {
        // 多种规格
        List<AppSpecificationValue> specificationValueList = appSpecificationValueMapper.selectList(
                new LambdaQueryWrapper<AppSpecificationValue>()
                        .eq(AppSpecificationValue::getProductId, productId)
        );

        // 单规格商品
        if (CollectionUtil.isEmpty(specificationValueList)) {
            specificationValueList = getSingleSpecification();
        }
        return specificationValueList;
    }

    private List<AppSpecificationValue> getSingleSpecification() {
        AppSpecificationValue appSpecificationValue = new AppSpecificationValue();
        appSpecificationValue.setSpec("规格");
        appSpecificationValue.setValue("默认规格");
        return ListUtil.toList(appSpecificationValue);
    }
}
