package com.flyduck.modules.admin.controller;

import com.flyduck.common.lang.Result;
import com.flyduck.manager.AppUploadManager;
import com.flyduck.modules.admin.service.AdminUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * AdminUploadController
 * </p>
 *
 * @author flyduck
 * @since 2024-06-27
 */
@RestController
@RequestMapping("/admin")
public class AdminUploadController {

    @Autowired
    private AdminUploadService adminUploadService;

    @PostMapping("/upload")
    public Result<List<String>> upload(MultipartFile[] pics) throws IOException {
        List<String> list = adminUploadService.upload(pics);
        return Result.success(list);
    }
}
