package com.flyduck.manager;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.common.exception.BusinessException;
import com.flyduck.entity.AppOrderItem;
import com.flyduck.entity.AppProduct;
import com.flyduck.entity.AppSkuStock;
import com.flyduck.mapper.AppProductMapper;
import com.flyduck.mapper.AppSkuStockMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * AppSkuStockManager
 * </p>
 *
 * @author flyduck
 * @since 2024-05-20
 */
@Component
public class AppSkuStockManager {

    @Resource
    private AppSkuStockMapper appSkuStockMapper;
    @Resource
    private AppProductMapper appProductMapper;

    /**
     * 根据spuId获取商品所有的sku(规格、库存、价格)
     * 如果spuId在app_sku_stock存在，说明是多规格商品，返回app_sku_stock的所有记录
     * 如果spuId在app_sku_stock不存在，说明是单规格商品，将spu作为一个sku返回
     * @param productId
     * @return
     */
    public List<AppSkuStock> getAppSkuStockListByProductId(Long productId) {
        //多规格商品
        List<AppSkuStock> appSkuStockList = appSkuStockMapper.selectList(
                new LambdaQueryWrapper<AppSkuStock>()
                        .eq(AppSkuStock::getProductId, productId)
        );

        //单规格商品
        if (CollectionUtil.isEmpty(appSkuStockList)) {
            appSkuStockList = ListUtil.toList(getSingleSkuStock(productId));
        }
        return appSkuStockList;
    }

    /**
     * 获取单个sku
     * 如果skuId != -1：返回app_sku_stock的记录
     * 如果skuId == -1：将spu作为一个sku返回
     * @param skuId
     * @param productId
     * @return
     */
    public AppSkuStock getSkuStock(Long skuId, Long productId) {
        if(skuId != -1){
            //多规格库存
            return appSkuStockMapper.selectById(skuId);
        }else {
            //单规格库存
            return getSingleSkuStock(productId);
        }
    }

    /**
     * 获取单规格商品的属性、库存、价格，封装到AppSkuStock
     * 价格、库存：从product取
     * sku：写死为默认规格
     * id：写死为-1
     * @param productId
     * @return
     */
    public AppSkuStock getSingleSkuStock(Long productId) {
        AppProduct appProduct = appProductMapper.selectById(productId);

        AppSkuStock appSkuStock = new AppSkuStock();
        appSkuStock.setPrice(appProduct.getPrice());
        appSkuStock.setStock(appProduct.getStock());
        appSkuStock.setSku("默认规格");
        appSkuStock.setId(-1L);
        return appSkuStock;
    }

    /**
     * 遍历订单明细减库存
     * 如果是单规格的商品：减product
     * 如果是多规格的商品：减sku_stock
     * @param appOrderItemList 订单明细
     */
    public void reduceStock(List<AppOrderItem> appOrderItemList) {
        for (AppOrderItem appOrderItem : appOrderItemList) {
            int count;
            if(appOrderItem.getSkuId() == -1){
                //默认规格
                count = appProductMapper.reduceStock(appOrderItem.getProductId(),appOrderItem.getQuantity());
            }else {
                //多规格
                count = appSkuStockMapper.reduceStock(appOrderItem.getSkuId(),appOrderItem.getQuantity());
            }
            if(count == 0){
                throw new BusinessException("库存不足");
            }
        }
    }

    /**
     * 遍历订单明细释放库存
     * 如果是单规格的商品：加product
     * 如果是多规格的商品：加sku_stock
     * @param appOrderItemList 订单明细
     */
    public void releaseStock(List<AppOrderItem> appOrderItemList) {
        for (AppOrderItem appOrderItem : appOrderItemList) {
            int count;
            if(appOrderItem.getSkuId() == -1){
                //默认规格
                count = appProductMapper.releaseStock(appOrderItem.getProductId(),appOrderItem.getQuantity());
            }else {
                //多规格
                count = appSkuStockMapper.releaseStock(appOrderItem.getSkuId(),appOrderItem.getQuantity());
            }
            if(count == 0){
                throw new BusinessException("释放库存失败");
            }
        }
    }
}
