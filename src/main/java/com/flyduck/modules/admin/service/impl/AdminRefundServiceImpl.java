package com.flyduck.modules.admin.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.exception.BusinessException;
import com.flyduck.entity.AppOrder;
import com.flyduck.entity.AppOrderItem;
import com.flyduck.entity.AppRefund;
import com.flyduck.manager.AppOrderManager;
import com.flyduck.manager.AppPayManager;
import com.flyduck.mapper.AppOrderItemMapper;
import com.flyduck.mapper.AppOrderMapper;
import com.flyduck.mapper.AppRefundMapper;
import com.flyduck.modules.admin.service.AdminRefundService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.modules.app.constant.AppConstant;
import com.flyduck.utils.BeanUtils;
import com.flyduck.utils.SecurityUtils;
import com.flyduck.utils.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * AdminRefundServiceImpl
 * </p>
 *
 * @author flyduck
 * @since 2024-06-13
 */
@Service
public class AdminRefundServiceImpl implements AdminRefundService {

    @Resource
    private AppRefundMapper appRefundMapper;
    @Resource
    private AppOrderMapper appOrderMapper;
    @Resource
    private AppPayManager appPayManager;
    @Resource
    private AppOrderItemMapper appOrderItemMapper;

    @Override
    public List<AdminDictRespVO> getRefundStatusList() {
        List<AdminDictRespVO> adminDictRespVOList = new ArrayList<>();
        for (AppConstant.RefundStatus refundStatus : AppConstant.RefundStatus.values()) {
            AdminDictRespVO adminDictRespVO = new AdminDictRespVO();
            adminDictRespVO.setKey(refundStatus.getCode());
            adminDictRespVO.setValue(refundStatus.getDescription());
            adminDictRespVOList.add(adminDictRespVO);
        }
        return adminDictRespVOList;
    }

    @Override
    public Page<AdminRefundRespVO> getRefundPage(Page page, AdminRefundQueryReqVO adminRefundQueryReqVO) {
        return appRefundMapper.getRefundPage(page,adminRefundQueryReqVO);
    }

    @Override
    public AdminRefundDetailsRespVO getRefundDetailsById(Long id) {
        AdminRefundDetailsRespVO adminRefundDetailsRespVO = new AdminRefundDetailsRespVO();

        //退货
        AppRefund appRefund = appRefundMapper.selectById(id);
        AdminRefundDetailsRespVO.AppRefundVO appRefundVO = BeanUtils.toBean(appRefund, AdminRefundDetailsRespVO.AppRefundVO.class);
        adminRefundDetailsRespVO.setRefund(appRefundVO);

        //订单
        AppOrder appOrder = appOrderMapper.selectById(appRefund.getOrderId());
        List<AppOrderItem> appOrderItemList = appOrderItemMapper.getAppOrderItemListByOrderId(appOrder.getId());
        AdminRefundDetailsRespVO.AppOrderVO appOrderVO = BeanUtils.toBean(appOrder, AdminRefundDetailsRespVO.AppOrderVO.class);
        List<AdminRefundDetailsRespVO.AppOrderItemVO> appOrderItemVOList = BeanUtils.toBeanList(appOrderItemList, AdminRefundDetailsRespVO.AppOrderItemVO.class);
        appOrderVO.setOrderItems(appOrderItemVOList);
        adminRefundDetailsRespVO.setOrder(appOrderVO);

        return adminRefundDetailsRespVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void auditRefund(AdminRefundAuditReqVO adminRefundAuditReqVO) {
        AppRefund appRefund = appRefundMapper.selectById(adminRefundAuditReqVO.getId());
        AppOrder appOrder = appOrderMapper.selectById(appRefund.getOrderId());

        if (AppConstant.RefundStatus.PROCESSING.getCode() == adminRefundAuditReqVO.getRefundStatus() //退货中
                || AppConstant.RefundStatus.REJECTED.getCode() == adminRefundAuditReqVO.getRefundStatus()) {//拒绝退货
            if(AppConstant.RefundStatus.PENDING.getCode() != appRefund.getRefundStatus()){
                throw new BusinessException("状态异常");
            }

            if(adminRefundAuditReqVO.getRefundAmount() == null || StringUtils.isBlank(adminRefundAuditReqVO.getOperateRemark())){
                throw new BusinessException("退款金额或处理备注不能为空");
            }

            if(adminRefundAuditReqVO.getRefundAmount().compareTo(appOrder.getTotalAmount()) > 0){
                throw new BusinessException("退款金额不能大于订单总金额");
            }

            appRefund.setRefundAmount(adminRefundAuditReqVO.getRefundAmount());
            appRefund.setRefundStatus(adminRefundAuditReqVO.getRefundStatus());
            appRefund.setOperateRemark(adminRefundAuditReqVO.getOperateRemark());
            appRefund.setOperatorId(SecurityUtils.getUserInfo().getId());
            appRefund.setOperateTime(LocalDateTime.now());

            //如果是拒绝
            if(AppConstant.RefundStatus.REJECTED.getCode() == adminRefundAuditReqVO.getRefundStatus()){
                appOrder.setOrderStatus(AppConstant.OrderStatus.REFUND_FAILED.getCode());
                appOrderMapper.updateById(appOrder);
            }

        }else if(AppConstant.RefundStatus.COMPLETED.getCode() == adminRefundAuditReqVO.getRefundStatus()){//确认退货退款
            if(AppConstant.RefundStatus.PROCESSING.getCode() != appRefund.getRefundStatus()){//必须是退货中
                throw new BusinessException("状态异常");
            }

            if(StringUtils.isBlank(adminRefundAuditReqVO.getReceiptRemark())){
                throw new BusinessException("处理备注不能为空");
            }

            appRefund.setReceiptRemark(adminRefundAuditReqVO.getReceiptRemark());
            appRefund.setRefundStatus(adminRefundAuditReqVO.getRefundStatus());


            appOrder.setOrderStatus(AppConstant.OrderStatus.REFUNDED.getCode());
            appOrder.setRefundTime(LocalDateTime.now());
            appOrderMapper.updateById(appOrder);

            //退款处理
            appPayManager.refund(appRefund.getId());
        }
        appRefund.setUpdated(LocalDateTime.now());
        appRefundMapper.updateById(appRefund);
    }
}
