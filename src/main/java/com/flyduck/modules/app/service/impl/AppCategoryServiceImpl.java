package com.flyduck.modules.app.service.impl;

import com.flyduck.entity.AppCategory;
import com.flyduck.mapper.AppCategoryMapper;
import com.flyduck.modules.app.service.AppCategoryService;
import com.flyduck.modules.app.vo.category.AppCategoryDetailsRespVO;
import com.flyduck.utils.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-16 10:13
 **/
@Service
public class AppCategoryServiceImpl implements AppCategoryService {

    @Resource
    private AppCategoryMapper appCategoryMapper;

    @Override
    public AppCategoryDetailsRespVO getCategoryDetailsById(Long id) {
        AppCategory appCategory = appCategoryMapper.selectById(id);
        return BeanUtils.toBean(appCategory, AppCategoryDetailsRespVO.class);
    }
}
