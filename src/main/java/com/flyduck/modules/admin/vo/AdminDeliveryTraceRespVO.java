package com.flyduck.modules.admin.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AdminDeliveryTraceRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-28
 */
@Data
public class AdminDeliveryTraceRespVO implements Serializable {
    private String acceptTime;
    private String acceptStation;
    private String remark;
}
