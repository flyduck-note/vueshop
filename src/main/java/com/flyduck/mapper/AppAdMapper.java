package com.flyduck.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.entity.AppAd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flyduck.utils.StringUtils;

import java.util.List;

/**
 * @Entity com.flyduck.entity.AppAd
 */
public interface AppAdMapper extends BaseMapper<AppAd> {

}




