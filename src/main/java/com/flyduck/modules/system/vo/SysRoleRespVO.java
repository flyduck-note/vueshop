package com.flyduck.modules.system.vo;

import com.flyduck.entity.SysRole;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * SysRoleRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysRoleRespVO extends SysRole implements Serializable {
}
