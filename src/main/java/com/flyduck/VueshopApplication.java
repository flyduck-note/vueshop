package com.flyduck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VueshopApplication {

    public static void main(String[] args) {
        SpringApplication.run(VueshopApplication.class, args);
    }

}
