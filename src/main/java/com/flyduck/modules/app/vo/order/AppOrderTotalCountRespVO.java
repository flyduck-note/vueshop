package com.flyduck.modules.app.vo.order;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AppOrderCountRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-05-22
 */
@Data
public class AppOrderTotalCountRespVO implements Serializable {
    private int unPay;//待付款
    private int unDeli;//待发货
    private int unDecv;//待收货
    private int unRefund;//退款中
    private long unComment;//待评论
}
