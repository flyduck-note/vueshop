package com.flyduck.config;

import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Collection;

public interface BaseMapperX<T> extends BaseMapper<T> {
    default void insertBatch(Collection<T> entities) {
        Db.saveBatch(entities);
    }

    default void updateBatchById(Collection<T> entities) {
        Db.updateBatchById(entities);
    }

    default void insertOrUpdateBatch(Collection<T> entities) {
        Db.saveOrUpdateBatch(entities);
    }


}
