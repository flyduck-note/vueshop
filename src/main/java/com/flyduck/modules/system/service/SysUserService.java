package com.flyduck.modules.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.system.vo.*;

import java.util.List;

/**
 * <p>
 * SysUserService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
public interface SysUserService {
    Page<SysUserRespVO> getUserPage(Page page, SysUserQueryReqVO sysUserQueryReqVO);

    SysUserDetailsRespVO getUserDetailsById(Long id);

    void saveUser(SysUserSaveReqVO sysUserSaveReqVO);

    void updateUser(SysUserUpdateReqVO sysUserUpdateReqVO);

    void batchDeleteByIds(List<Long> ids);

    void assignRolesToUser(Long userId, List<Long> roleIds);

    void resetPasswordById(Long userId);
}
