package com.flyduck.modules.app.mq;

import com.flyduck.manager.AppOrderManager;
import com.flyduck.modules.app.config.RabbitConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * <p>
 * MqReceiver
 * </p>
 *
 * @author flyduck
 * @since 2024-05-21
 */
@Slf4j
@Component
public class MqReceiver {

    @Resource
    private AppOrderManager appOrderManager;

//    @RabbitListener(queues = {RabbitConfig.TTL_ORDER_CANCEL_QUEUE})
    public void cancelOrderHandler(long orderId){
        //取消订单
        log.info("正在取消订单={}",orderId);
        appOrderManager.closeOrder(orderId, "支付超时自动取消",2L);
    }
}
