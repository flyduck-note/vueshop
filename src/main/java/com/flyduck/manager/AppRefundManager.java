package com.flyduck.manager;

import cn.hutool.core.util.RandomUtil;
import com.flyduck.utils.DateUtils;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-23 21:26
 **/
@Component
public class AppRefundManager {
    /**
     * 生成订单的sn
     * @return
     */
    public String generateRefundSn() {
        String dateStr = DateUtils.format(new Date(), DateUtils.YYYYMMDDHHMM);
        return "T" + dateStr + RandomUtil.randomNumbers(4);
    }
}
