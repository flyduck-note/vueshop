package com.flyduck.modules.app.controller;

import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.annotation.Login;
import com.flyduck.modules.app.service.AppAddressService;
import com.flyduck.modules.app.vo.address.AppAddressDetailsRespVO;
import com.flyduck.modules.app.vo.address.AppAddressRespVO;
import com.flyduck.modules.app.vo.address.AppAddressSaveReqVO;
import com.flyduck.modules.app.vo.address.AppAddressUpdateReqVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * AppAddressController
 * </p>
 *
 * @author flyduck
 * @since 2024-06-19
 */
@RestController
@RequestMapping("/app/address")
public class AppAddressController {

    @Resource
    private AppAddressService appAddressService;

    @Login
    @GetMapping("/getAddressList")
    public Result<List<AppAddressRespVO>> getAddressList(){
        List<AppAddressRespVO> addressRespVOList = appAddressService.getAddressList();
        return Result.success(addressRespVOList);
    }

    @Login
    @GetMapping("/getAddressDetailsById/{id}")
    public Result<AppAddressDetailsRespVO> getAddressDetailsById(@PathVariable("id") Long id){
        AppAddressDetailsRespVO appAddressDetailsRespVO = appAddressService.getAddressDetailsById(id);
        return Result.success(appAddressDetailsRespVO);
    }

    @Login
    @PostMapping("/saveAddress")
    public Result<Void> saveAddress(@Validated @RequestBody AppAddressSaveReqVO appAddressSaveReqVO){
        appAddressService.saveAddress(appAddressSaveReqVO);
        return Result.success();
    }

    @Login
    @PostMapping("/updateAddress")
    public Result<Void> updateAddress(@Validated @RequestBody AppAddressUpdateReqVO appAddressUpdateReqVO){
        appAddressService.updateAddress(appAddressUpdateReqVO);
        return Result.success();
    }

    @Login
    @PostMapping("/deleteById/{id}")
    public Result<Void> deleteById(@PathVariable("id") Long id){
        appAddressService.deleteById(id);
        return Result.success();
    }

}
