package com.flyduck.modules.app.vo.product;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.entity.AppCategory;
import com.flyduck.entity.AppProduct;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 21:54
 **/
@Data
public class AppProductRespVO extends AppProduct implements Serializable {
}
