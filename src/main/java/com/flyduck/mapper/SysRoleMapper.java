package com.flyduck.mapper;

import com.flyduck.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity com.flyduck.entity.SysRole
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {
    List<SysRole> getRoleListByUserId(@Param("userId") Long userId);

}




