package com.flyduck.modules.system.security;

import cn.hutool.core.collection.CollectionUtil;
import com.flyduck.entity.SysUser;
import com.flyduck.mapper.SysUserMapper;
import com.flyduck.modules.system.constant.SysConstant;
import com.flyduck.dto.SysUserInfoDTO;
import com.flyduck.manager.SysPermissionCacheManager;
import com.flyduck.utils.BeanUtils;
import com.flyduck.utils.JwtUtils;
import com.flyduck.utils.StringUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: vue-admin-java
 * @description: 其他请求携带token，这里解析token完成自动登录
 * @author: flyduck
 * @create: 2024-05-28 21:45
 **/
public class TokenAuthenticationFilter extends BasicAuthenticationFilter {

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private SysPermissionCacheManager sysPermissionCacheManager;

    @Autowired
    private SysUserMapper sysUserMapper;

    public TokenAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String token = request.getHeader(SysConstant.TOKEN_HEADER);
        //没有token，直接跳过
        if (StringUtils.isBlank(token)) {
            chain.doFilter(request,response);
            return;
        }

        //有token，解析
        Claims claims = jwtUtils.getClaimByToken(token,"ADMIN");
        if (claims == null) {
            throw new JwtException("token异常");
        }
        if(jwtUtils.isTokenExpired(claims.getExpiration())){
            throw new JwtException("token已过期");
        }

        String userId = claims.getSubject();

        SysUser sysUser = sysUserMapper.selectById(userId);

        if (sysUser == null) {
            throw new UsernameNotFoundException("用户名或密码不正确");
        }

        if (SysConstant.CommonStatus.DISABLE.getCode() == sysUser.getStatus()) {
            throw new LockedException("账户被锁定");
        }

        SysUserInfoDTO sysUserInfoDTO = BeanUtils.toBean(sysUser, SysUserInfoDTO.class);

        List<String> permissionList = sysPermissionCacheManager.getPermissionListByUserId(sysUser.getId());
        List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
        if(CollectionUtil.isNotEmpty(permissionList)){
            authorityList = permissionList.stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
        }

        //获取用户的权限等信息
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(sysUserInfoDTO, null,authorityList);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        chain.doFilter(request,response);
    }
}
