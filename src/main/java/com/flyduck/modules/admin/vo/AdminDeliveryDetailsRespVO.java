package com.flyduck.modules.admin.vo;

import com.flyduck.modules.app.vo.delivery.AppDeliveryInfoRespVO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * AdminDeliveryDetailsRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-13
 */
@Data
public class AdminDeliveryDetailsRespVO implements Serializable{
    private List<AppDeliveryInfoRespVO.AppDeliveryInfoVO> deliveryInfo;

    private String deliveryCompany;

    private String deliverySn;

    @Data
    public static class AppDeliveryInfoVO implements Serializable {
        private String acceptTime;
        private String acceptStation;
        private String remark;
    }
}
