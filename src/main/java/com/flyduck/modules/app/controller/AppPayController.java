package com.flyduck.modules.app.controller;

import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.annotation.Login;
import com.flyduck.modules.app.annotation.NoRepeatSubmit;
import com.flyduck.modules.app.service.AppPayService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * <p>
 * AppPayController
 * </p>
 *
 * @author flyduck
 * @since 2024-05-22
 */
@RestController
@RequestMapping("/app/pay")
public class AppPayController {

    @Resource
    private AppPayService appPayService;

    @Login
    @NoRepeatSubmit(lockTime = 5)
    @PostMapping("/orderPayByOrderSn")
    public Result<String> orderPayByOrderSn(String orderSn){
        return Result.success(appPayService.orderPayByOrderSn(orderSn));
    }

    @Login
    @PostMapping("/payCheckByOrderSn")
    public Result<String> payCheckByOrderSn(String orderSn){
        return Result.success(appPayService.payCheckByOrderSn(orderSn));
    }

    @PostMapping("/payNotify")
    public void payNotify(@RequestParam Map<String,String> paramMap, HttpServletResponse response) throws IOException {
        appPayService.payNotify(paramMap,response);
    }
}
