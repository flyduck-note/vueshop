package com.flyduck.modules.admin.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AdminOrderQueryVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-11
 */
@Data
public class AdminOrderQueryReqVO implements Serializable {
    private String sn;
    private Integer orderStatus;
    private String username;
}
