package com.flyduck.modules.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.admin.service.AdminCategoryService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.modules.app.controller.BaseController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * SysMenuController
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@RestController
@RequestMapping("/admin/category")
public class AdminCategoryController extends BaseController {

    @Resource
    private AdminCategoryService sysCategoryService;

    @GetMapping("/getCategoryList")
    public Result<List<AdminCategoryRespVO>> getCategoryList(){
        List<AdminCategoryRespVO> adminCategoryRespVOList = sysCategoryService.getCategoryList();
        return Result.success(adminCategoryRespVOList);
    }

    @GetMapping("/getCategoryPage")
    public Result<Page<AdminCategoryRespVO>> getCategoryPage(AdminCategoryQueryReqVO sysCategoryQueryReqVO){
        Page<AdminCategoryRespVO> sysCategoryRespVOPage = sysCategoryService.getCategoryPage(getPage(),sysCategoryQueryReqVO);
        return Result.success(sysCategoryRespVOPage);
    }

    @GetMapping("/getCategoryDetailsById/{id}")
    public Result<AdminCategoryDetailsRespVO> getCategoryDetailsById(@PathVariable("id") Long id){
        AdminCategoryDetailsRespVO sysCategoryDetailsRespVO = sysCategoryService.getCategoryDetailsById(id);
        return Result.success(sysCategoryDetailsRespVO);
    }

    @PostMapping("/saveCategory")
    public Result<Void> saveCategory(@Validated @RequestBody AdminCategorySaveReqVO adminCategorySaveReqVO){
        sysCategoryService.saveCategory(adminCategorySaveReqVO);
        return Result.success();
    }

    @PostMapping("/updateCategory")
    public Result<Void> updateCategory(@Validated @RequestBody AdminCategoryUpdateReqVO adminCategoryUpdateReqVO){
        sysCategoryService.updateCategory(adminCategoryUpdateReqVO);
        return Result.success();
    }

    @PostMapping("/batchDeleteByIds")
    public Result<Void> batchDeleteByIds(@RequestBody Long[] ids){
        sysCategoryService.batchDeleteByIds(Arrays.asList(ids));
        return Result.success();
    }


}
