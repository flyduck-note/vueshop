package com.flyduck.modules.app.controller;

import cn.hutool.core.collection.ListUtil;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.annotation.Login;
import com.flyduck.modules.app.vo.cartItem.AppCartItemAddReqVO;
import com.flyduck.modules.app.vo.cartItem.AppCartItemRespVO;
import com.flyduck.modules.app.service.AppCartItemService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-19 09:26
 **/
@RestController
@RequestMapping("/app/cartItem")
public class AppCartItemController {

    @Resource
    private AppCartItemService appCartItemService;

    @Login
    @GetMapping("/getCartItemList")
    public Result<List<AppCartItemRespVO>> getCartItemList(){
        List<AppCartItemRespVO> cartItemResponseList = appCartItemService.getCartItemList();
        return Result.success(cartItemResponseList);
    }

    @Login
    @GetMapping("/getTotalCount")
    public Result<Long> getTotalCount(){
        Long count = appCartItemService.getTotalCount();
        return Result.success(count);
    }

    @Login
    @PostMapping("/getTotalAmountByIds")
    public Result<BigDecimal> getTotalAmountByIds(@RequestBody Long[] ids){
        BigDecimal totalAmount = appCartItemService.getTotalAmountByIds(ListUtil.toList(ids));
        return Result.success(totalAmount);
    }

    @Login
    @PostMapping("/updateQuantityById")
    public Result<Void> updateQuantity(Long id,Integer quantity){
        appCartItemService.updateQuantityById(id,quantity);
        return Result.success();
    }

    @Login
    @PostMapping("addCartItem")
    public Result<Void> addCartItem(@Validated @RequestBody AppCartItemAddReqVO appCartItemAddReqVO){
        appCartItemService.addCartItem(appCartItemAddReqVO);
        return Result.success();
    }

    @Login
    @PostMapping("batchDeleteByIds")
    public Result<Void> batchDeleteByIds(@RequestBody Long[] ids){
        appCartItemService.batchDeleteByIds(ids);
        return Result.success();
    }
}
