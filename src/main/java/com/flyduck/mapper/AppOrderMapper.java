package com.flyduck.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.entity.AppOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flyduck.modules.admin.vo.AdminOrderQueryReqVO;
import com.flyduck.modules.admin.vo.AdminOrderRespVO;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity com.flyduck.entity.AppOrder
 */
public interface AppOrderMapper extends BaseMapper<AppOrder> {

    default AppOrder getAppOrderBySn(String orderSn) {
        return this.selectOne(
                new LambdaQueryWrapper<AppOrder>()
                        .eq(AppOrder::getSn, orderSn)
        );
    }

    Page<AdminOrderRespVO> getOrderPage(@Param("page") Page page, @Param("adminOrderQueryReqVO") AdminOrderQueryReqVO adminOrderQueryReqVO);

}




