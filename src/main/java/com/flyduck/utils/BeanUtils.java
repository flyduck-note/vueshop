package com.flyduck.utils;


import cn.hutool.core.collection.CollectionUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * BeanUtils
 * </p>
 *
 * @author flyduck
 * @since 2024/5/9
 */
public class BeanUtils extends org.springframework.beans.BeanUtils {

    public static <T> T toBean(Object source,Class<T> clazz){
        if (source == null) {
            return null;
        }
        T newInstance = null;
        try {
            newInstance = clazz.newInstance();
            copyProperties(source, newInstance);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newInstance;
    }

    public static <S,T> List<T> toBeanList(List<S> sourceList, Class<T> clazz){
        if (CollectionUtil.isEmpty(sourceList)) {
            return new ArrayList<>();
        }
        return sourceList.stream().map(source -> toBean(source, clazz)).collect(Collectors.toList());
    }

}
