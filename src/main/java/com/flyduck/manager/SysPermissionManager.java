package com.flyduck.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.entity.SysMenu;
import com.flyduck.entity.SysRole;
import com.flyduck.mapper.SysMenuMapper;
import com.flyduck.mapper.SysRoleMapper;
import com.flyduck.modules.system.constant.SysConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * SysPermissionManager
 * </p>
 *
 * @author flyduck
 * @since 2024-05-29
 */
@Component
public class SysPermissionManager {

    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SysMenuMapper sysMenuMapper;

    public List<String> getPermissionListByUserId(Long userId) {
        List<SysMenu> sysMenuList;
        if(userId == 1L){
            //管理员
            sysMenuList = sysMenuMapper.selectList(
                    new LambdaQueryWrapper<SysMenu>()
                            .eq(SysMenu::getStatus, SysConstant.CommonStatus.ENABLE.getCode())
            );
        }else {
            //非管理员
            sysMenuList = sysMenuMapper.getEnableMenuListByUserId(userId);
        }

        List<String> permissionList = sysMenuList.stream()
                .map(SysMenu::getPerms)
                .collect(Collectors.toList());
        return permissionList;
    }
}
