package com.flyduck.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * SysUserInfoDTO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysUserInfoDTO implements Serializable {

    private Long id;
    private String username;
    private String avatar;
}
