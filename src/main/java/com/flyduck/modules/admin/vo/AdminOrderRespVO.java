package com.flyduck.modules.admin.vo;

import com.flyduck.entity.AppOrder;
import com.flyduck.modules.app.constant.AppConstant;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AdminOrderRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-11
 */
@Data
public class AdminOrderRespVO extends AppOrder implements Serializable {


    private String username;
    private String userAvatar;

    //订单状态中文
    private String orderStatusStr;

    public String getOrderStatusStr() {
        return AppConstant.OrderStatus.fromCode(getOrderStatus()).getDescription();
    }
}
