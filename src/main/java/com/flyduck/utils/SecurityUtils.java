package com.flyduck.utils;

import com.flyduck.dto.SysUserInfoDTO;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * <p>
 * SecurityUtils
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
public class SecurityUtils {

    public static SysUserInfoDTO getUserInfo(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof SysUserInfoDTO) {
            SysUserInfoDTO sysUserInfoDTO = (SysUserInfoDTO) principal;
            return sysUserInfoDTO;
        }else {
            return null;
        }
    }
}
