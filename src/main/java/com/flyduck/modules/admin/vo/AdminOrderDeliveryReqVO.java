package com.flyduck.modules.admin.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AdminOrderDeliveryReqVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-12
 */
@Data
public class AdminOrderDeliveryReqVO implements Serializable {
    private Long id;
    private String deliveryCompany;
    private String deliverySn;
}
