package com.flyduck.modules.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.entity.AppAd;
import com.flyduck.entity.AppCategory;
import com.flyduck.entity.AppProduct;
import com.flyduck.mapper.AppAdMapper;
import com.flyduck.mapper.AppCategoryMapper;
import com.flyduck.mapper.AppProductMapper;
import com.flyduck.modules.app.constant.AppConstant;
import com.flyduck.modules.app.vo.home.AppHomeDataRespVO;
import com.flyduck.modules.app.service.AppHomeService;
import com.flyduck.utils.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 18:49
 **/
@Service
public class AppHomeServiceImpl implements AppHomeService {

    @Resource
    private AppAdMapper appAdMapper;
    @Resource
    private AppCategoryMapper appCategoryMapper;
    @Resource
    private AppProductMapper appProductMapper;

    @Override
    public AppHomeDataRespVO getHomeData() {
        AppHomeDataRespVO appHomeDataRespVO = new AppHomeDataRespVO();

        // 首页轮播图
        List<AppAd> appAdList = appAdMapper.selectList(
                new LambdaQueryWrapper<AppAd>()
                .eq(AppAd::getPosition, AppConstant.AppAdPosition.carousel.getCode())
                .eq(AppAd::getIsShow,true)
                .orderByDesc(AppAd::getSortOrder)
        );
        List<AppHomeDataRespVO.AppAdVO> appAdVOList = BeanUtils.toBeanList(appAdList, AppHomeDataRespVO.AppAdVO.class);
        appHomeDataRespVO.setCarousels(appAdVOList);


        // 分类数据
        List<AppCategory> appCategoryList = appCategoryMapper.selectList(
                new LambdaQueryWrapper<AppCategory>()
                .eq(AppCategory::getIsShow,true)
                .orderByDesc(AppCategory::getSortOrder)
        );
        List<AppHomeDataRespVO.AppCategoryVO> categories = BeanUtils.toBeanList(appCategoryList, AppHomeDataRespVO.AppCategoryVO.class);
        appHomeDataRespVO.setCategories(categories);


        // 置顶或者热销商品
        List<AppProduct> appProductList = appProductMapper.selectList(
                new LambdaQueryWrapper<AppProduct>()
                        .eq(AppProduct::getIsOnSale, true)
                        .and(wrapper -> wrapper.eq(AppProduct::getIsHot, true).or().eq(AppProduct::getIsTop, true))
                        .orderByDesc(AppProduct::getSortOrder)
                        .last("limit 10")
        );
        List<AppHomeDataRespVO.AppProductVO> appProductVOS = BeanUtils.toBeanList(appProductList, AppHomeDataRespVO.AppProductVO.class);
        appHomeDataRespVO.setProducts(appProductVOS);
        return appHomeDataRespVO;
    }
}
