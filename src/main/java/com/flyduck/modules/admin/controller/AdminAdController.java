package com.flyduck.modules.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.admin.service.AdminAdService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.modules.app.controller.BaseController;
import com.flyduck.modules.system.vo.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:39
 **/
@RestController
@RequestMapping("/admin/ad")
public class AdminAdController extends BaseController {

    @Resource
    private AdminAdService adminAdService;

    @GetMapping("/getAdPage")
    public Result<Page<AdminAdRespVO>> getAdPage(AdminAdQueryReqVO adminAdQueryReqVO){
        Page<AdminAdRespVO> adminAdRespVOPage = adminAdService.getAdPage(getPage(),adminAdQueryReqVO);
        return Result.success(adminAdRespVOPage);
    }

    @GetMapping("/getAdDetailsById/{id}")
    public Result<AdminAdDetailsRespVO> getAdDetailsById(@PathVariable("id") Long id){
        AdminAdDetailsRespVO adminAdDetailsRespVO = adminAdService.getAdDetailsById(id);
        return Result.success(adminAdDetailsRespVO);
    }

    @PostMapping("/saveAd")
    public Result<Void> saveAd(@Validated @RequestBody AdminAdSaveReqVO adminAdSaveReqVO){
        adminAdService.saveAd(adminAdSaveReqVO);
        return Result.success();
    }

    @PostMapping("/updateAd")
    public Result<Void> updateAd(@Validated @RequestBody AdminAdUpdateReqVO adminAdUpdateReqVO){
        adminAdService.updateAd(adminAdUpdateReqVO);
        return Result.success();
    }

    @PostMapping("/batchDeleteByIds")
    public Result<Void> batchDeleteByIds(@RequestBody Long[] ids){
        adminAdService.batchDeleteByIds(Arrays.asList(ids));
        return Result.success();
    }
}
