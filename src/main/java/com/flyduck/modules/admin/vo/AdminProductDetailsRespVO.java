package com.flyduck.modules.admin.vo;

import com.flyduck.entity.AppProduct;
import com.flyduck.entity.AppSkuStock;
import com.flyduck.entity.AppSpecificationValue;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:48
 **/
@Data
public class AdminProductDetailsRespVO extends AppProduct implements Serializable {
    private List<AdminSpecificationValueVO> specifications;
    private List<AdminSkuStockVO> skuStocks;

    @Data
    public static class AdminSpecificationValueVO extends AppSpecificationValue implements Serializable {

    }

    @Data
    public static class AdminSkuStockVO extends AppSkuStock implements Serializable{

    }
}
