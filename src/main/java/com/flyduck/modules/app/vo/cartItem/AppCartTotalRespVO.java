package com.flyduck.modules.app.vo.cartItem;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-19 16:24
 **/
@Data
public class AppCartTotalRespVO implements Serializable {

    private BigDecimal total;//总金额
}
