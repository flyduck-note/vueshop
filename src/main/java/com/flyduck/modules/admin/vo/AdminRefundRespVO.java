package com.flyduck.modules.admin.vo;

import com.flyduck.entity.AppRefund;
import com.flyduck.modules.app.constant.AppConstant;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AdminRefundRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-13
 */
@Data
public class AdminRefundRespVO extends AppRefund implements Serializable {

    private String refundStatusStr;
    private String username;
    private String userAvatar;

    public String getRefundStatusStr() {
        return AppConstant.RefundStatus.fromCode(getRefundStatus()).getDescription();
    }
}
