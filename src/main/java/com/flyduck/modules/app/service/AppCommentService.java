package com.flyduck.modules.app.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.app.vo.comment.AppCommentDetailsRespVO;
import com.flyduck.modules.app.vo.comment.AppCommentProductRespVO;
import com.flyduck.modules.app.vo.comment.AppCommentRespVO;
import com.flyduck.modules.app.vo.comment.AppCommentPostReqVO;
import org.springframework.web.multipart.MultipartFile;

public interface AppCommentService {
    Page<AppCommentRespVO> getCommentPageByProductId(Page page, Long productId);

    Page<AppCommentProductRespVO> getCommentProductPageByStatus(Page page, Integer status);

    Long postComment(MultipartFile[] pics, AppCommentPostReqVO appCommentPostReqVO);

    AppCommentDetailsRespVO getCommentDetailsById(Long id);
}
