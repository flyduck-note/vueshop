package com.flyduck.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.flyduck.entity.AppCartItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flyduck.dto.AppCartItemDTO;
import com.flyduck.modules.app.vo.cartItem.AppCartItemRespVO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Entity com.flyduck.entity.AppCartItem
 */
public interface AppCartItemMapper extends BaseMapper<AppCartItem> {

    List<AppCartItemDTO> getCartItemList(@Param(Constants.WRAPPER) QueryWrapper<AppCartItem> queryWrapper);

    List<AppCartItemRespVO> getCartItemListByUserId(@Param("userId") Long userId);

    BigDecimal getTotalAmountByIdsAndUserId(@Param("ids") ArrayList<Long> ids, @Param("userId") Long userId);
}




