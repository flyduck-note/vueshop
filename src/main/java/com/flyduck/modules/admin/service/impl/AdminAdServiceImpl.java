package com.flyduck.modules.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.entity.AppAd;
import com.flyduck.entity.SysRole;
import com.flyduck.mapper.AppAdMapper;
import com.flyduck.modules.admin.service.AdminAdService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.modules.system.vo.SysRoleRespVO;
import com.flyduck.utils.BeanUtils;
import com.flyduck.utils.PageUtils;
import com.flyduck.utils.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:44
 **/
@Service
public class AdminAdServiceImpl implements AdminAdService {

    @Resource
    private AppAdMapper appAdMapper;

    @Override
    public Page<AdminAdRespVO> getAdPage(Page page, AdminAdQueryReqVO adminAdQueryReqVO) {
        Page<AppAd> appAdPage = appAdMapper.selectPage(page,
                new LambdaQueryWrapper<AppAd>()
                        .like(StringUtils.isNotBlank(adminAdQueryReqVO.getName()), AppAd::getName, adminAdQueryReqVO.getName())
                        .orderByDesc(AppAd::getCreated)
        );

        return PageUtils.convert(appAdPage, AdminAdRespVO.class);
    }

    @Override
    public AdminAdDetailsRespVO getAdDetailsById(Long id) {
        AppAd appAd = appAdMapper.selectById(id);
        return BeanUtils.toBean(appAd, AdminAdDetailsRespVO.class);
    }

    @Override
    public void saveAd(AdminAdSaveReqVO adminAdSaveReqVO) {
        AppAd appAd = BeanUtils.toBean(adminAdSaveReqVO, AppAd.class);
        appAd.setCreated(LocalDateTime.now());
        appAdMapper.insert(appAd);
    }

    @Override
    public void updateAd(AdminAdUpdateReqVO adminAdUpdateReqVO) {
        AppAd appAd = BeanUtils.toBean(adminAdUpdateReqVO, AppAd.class);
        appAd.setUpdated(LocalDateTime.now());
        appAdMapper.updateById(appAd);
    }

    @Override
    public void batchDeleteByIds(List<Long> ids) {
        appAdMapper.deleteBatchIds(ids);
    }
}
