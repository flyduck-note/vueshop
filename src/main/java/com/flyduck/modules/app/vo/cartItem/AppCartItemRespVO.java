package com.flyduck.modules.app.vo.cartItem;

import com.flyduck.entity.AppCartItem;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-19 09:31
 **/
@Data
public class AppCartItemRespVO extends AppCartItem implements Serializable {

    private String productName;
    private String productImage;

    private String sku;
    private BigDecimal price;

}
