package com.flyduck.modules.app.vo.product;

import com.flyduck.entity.AppProduct;
import com.flyduck.entity.AppSkuStock;
import com.flyduck.entity.AppSpecificationValue;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 22:22
 **/
@Data
public class AppProductDetailsRespVO extends AppProduct implements Serializable {

    private List<AppSpecificationValueVO> specifications;
    private List<AppSkuStockVO> skuStocks;

    @Data
    public static class AppSpecificationValueVO extends AppSpecificationValue implements Serializable {

    }

    @Data
    public static class AppSkuStockVO extends AppSkuStock implements Serializable{

    }
}
