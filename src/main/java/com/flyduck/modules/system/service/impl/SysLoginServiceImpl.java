package com.flyduck.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.entity.SysMenu;
import com.flyduck.mapper.SysMenuMapper;
import com.flyduck.modules.system.constant.SysConstant;
import com.flyduck.dto.SysMenuDTO;
import com.flyduck.manager.SysPermissionCacheManager;
import com.flyduck.dto.SysUserInfoDTO;
import com.flyduck.modules.system.service.SysLoginService;
import com.flyduck.modules.system.vo.SysUserInfoRespVO;
import com.flyduck.utils.BeanUtils;
import com.flyduck.utils.SecurityUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-25 09:19
 **/
@Service
public class SysLoginServiceImpl implements SysLoginService {

    @Resource
    private SysPermissionCacheManager sysPermissionCacheManager;
    @Resource
    private SysMenuMapper sysMenuMapper;

    @Override
    public SysUserInfoRespVO getUserInfo() {
        //1、用户信息
        SysUserInfoDTO sysUserInfoDTO = SecurityUtils.getUserInfo();
        SysUserInfoRespVO.SysUserInfoVO sysUserInfoVO = BeanUtils.toBean(sysUserInfoDTO, SysUserInfoRespVO.SysUserInfoVO.class);

        //2、菜单信息
        List<SysMenu> sysMenuList;
        if(sysUserInfoDTO.getId() == 1L){
            //管理员
            sysMenuList = sysMenuMapper.selectList(
                    new LambdaQueryWrapper<SysMenu>()
                            .eq(SysMenu::getStatus, SysConstant.CommonStatus.ENABLE.getCode())
                            .orderByDesc(SysMenu::getSortOrder)
            );
        }else {
            //非管理员
            sysMenuList = sysMenuMapper.getEnableMenuListByUserId(sysUserInfoDTO.getId());
        }

        List<SysUserInfoRespVO.SysMenuVO> sysMenuVOList = convertSysMenuToSysMenuVO(sysMenuList);
        List<SysUserInfoRespVO.SysMenuVO> sysMenuVOTree = buildTreeMenu(sysMenuVOList);

        //3、权限信息
        List<String> permissionList = sysPermissionCacheManager.getPermissionListByUserId(sysUserInfoDTO.getId());

        SysUserInfoRespVO sysUserInfoRespVO = new SysUserInfoRespVO();
        sysUserInfoRespVO.setUserInfo(sysUserInfoVO);
        sysUserInfoRespVO.setMenuList(sysMenuVOTree);
        sysUserInfoRespVO.setPermissionList(permissionList);
        return sysUserInfoRespVO;
    }

    //构建树状结构
    private List<SysUserInfoRespVO.SysMenuVO> buildTreeMenu(List<SysUserInfoRespVO.SysMenuVO> sysMenuDTOList) {
        List<SysUserInfoRespVO.SysMenuVO> sysMenuVOTree = new ArrayList<>();

        for (SysUserInfoRespVO.SysMenuVO sysMenuVO : sysMenuDTOList) {
            //找出sysMenuDTO的子集，并赋值给children
            for (SysUserInfoRespVO.SysMenuVO menuVO : sysMenuDTOList) {
                if(sysMenuVO.getId().equals(menuVO.getParentId())){
                    sysMenuVO.getChildren().add(menuVO);
                }

            }
            //提取目录作为顶级
            if (SysConstant.MenuType.DIRECTORY.getCode() == sysMenuVO.getType()) {
                sysMenuVOTree.add(sysMenuVO);
            }
        }
        return sysMenuVOTree;

    }

    private List<SysUserInfoRespVO.SysMenuVO> convertSysMenuToSysMenuVO(List<SysMenu> sysMenuList) {
        return sysMenuList.stream()
                .map(sysMenu -> {
                    SysUserInfoRespVO.SysMenuVO sysMenuVO = new SysUserInfoRespVO.SysMenuVO();
                    sysMenuVO.setId(sysMenu.getId());
                    sysMenuVO.setParentId(sysMenu.getParentId());
                    sysMenuVO.setType(sysMenu.getType());
                    sysMenuVO.setTitle(sysMenu.getTitle());
                    sysMenuVO.setIcon(sysMenu.getIcon());
                    sysMenuVO.setPath(sysMenu.getPath());
                    sysMenuVO.setComponent(sysMenu.getComponent());
                    sysMenuVO.setName(sysMenu.getPerms());
                    return sysMenuVO;
                }).collect(Collectors.toList());
    }
}
