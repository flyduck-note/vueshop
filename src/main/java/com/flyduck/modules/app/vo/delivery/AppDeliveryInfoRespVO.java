package com.flyduck.modules.app.vo.delivery;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * AppDeliveryInfoRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-05-23
 */
@Data
public class AppDeliveryInfoRespVO implements Serializable {

    private List<AppDeliveryInfoVO> deliveryInfo;

    private String deliveryCompany;

    private String deliverySn;

    @Data
    public static class AppDeliveryInfoVO implements Serializable{
        private String acceptTime;
        private String acceptStation;
        private String remark;
    }
}
