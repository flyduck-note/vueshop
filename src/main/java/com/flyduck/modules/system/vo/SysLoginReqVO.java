package com.flyduck.modules.system.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * LoginRequest
 * </p>
 *
 * @author flyduck
 * @since 2024/5/9
 */
@Data
public class SysLoginReqVO implements Serializable {

    @NotBlank(message = "用户名不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;
}
