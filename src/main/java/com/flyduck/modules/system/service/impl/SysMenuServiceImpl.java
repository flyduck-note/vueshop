package com.flyduck.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.common.exception.BusinessException;
import com.flyduck.entity.SysMenu;
import com.flyduck.entity.SysRoleMenu;
import com.flyduck.mapper.SysMenuMapper;
import com.flyduck.mapper.SysRoleMenuMapper;
import com.flyduck.modules.system.constant.SysConstant;
import com.flyduck.manager.SysPermissionCacheManager;
import com.flyduck.modules.system.service.SysMenuService;
import com.flyduck.modules.system.vo.SysMenuDetailsRespVO;
import com.flyduck.modules.system.vo.SysMenuSaveReqVO;
import com.flyduck.modules.system.vo.SysMenuTreeRespVO;
import com.flyduck.modules.system.vo.SysMenuUpdateReqVO;
import com.flyduck.utils.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * SysMenuServiceImpl
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Service
public class SysMenuServiceImpl implements SysMenuService {

    @Resource
    private SysMenuMapper sysMenuMapper;
    @Resource
    private SysPermissionCacheManager sysPermissionCacheManager;
    @Resource
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    public List<SysMenuTreeRespVO> getMenuTree() {
        //获取所有菜单信息
        List<SysMenu> sysMenuList = sysMenuMapper.selectList(
                new LambdaQueryWrapper<SysMenu>()
                        .orderByDesc(SysMenu::getSortOrder)
        );
        List<SysMenuTreeRespVO> sysMenuTreeRespVOList = BeanUtils.toBeanList(sysMenuList, SysMenuTreeRespVO.class);

        //转成树状结构
        List<SysMenuTreeRespVO> treeMenu = buildMenuTree(sysMenuTreeRespVOList);
        return treeMenu;
    }

    private List<SysMenuTreeRespVO> buildMenuTree(List<SysMenuTreeRespVO> sysMenuTreeRespVOList) {
        // 各自找到自己的孩子（目录的孩子是菜单，菜单的孩子是按钮），如果是目录放到treeMenu里面
        List<SysMenuTreeRespVO>  treeMenu = new ArrayList<>();
        for (SysMenuTreeRespVO directory : sysMenuTreeRespVOList) {
            for (SysMenuTreeRespVO menu : sysMenuTreeRespVOList) {
                if(menu.getParentId().equals(directory.getId())){
                    directory.getChildren().add(menu);
                }
            }
            if(SysConstant.MenuType.DIRECTORY.getCode() == directory.getType()){
                treeMenu.add(directory);
            }
        }
        return treeMenu;
    }

    @Override
    public SysMenuDetailsRespVO getMenuDetailsById(Long id) {
        SysMenu sysMenu = sysMenuMapper.selectById(id);
        SysMenuDetailsRespVO sysMenuDetailsRespVO = BeanUtils.toBean(sysMenu, SysMenuDetailsRespVO.class);
        return sysMenuDetailsRespVO;
    }

    @Override
    public void saveMenu(SysMenuSaveReqVO sysMenuSaveReqVO) {
        SysMenu sysMenu = BeanUtils.toBean(sysMenuSaveReqVO, SysMenu.class);
        sysMenu.setCreated(LocalDateTime.now());
        sysMenuMapper.insert(sysMenu);
    }

    @Override
    public void updateMenu(SysMenuUpdateReqVO sysMenuUpdateReqVO) {
        // 清除所有该菜单相关的权限
        sysPermissionCacheManager.clearPermissionListByMenuId(sysMenuUpdateReqVO.getId());
        SysMenu sysMenu = BeanUtils.toBean(sysMenuUpdateReqVO, SysMenu.class);
        sysMenu.setUpdated(LocalDateTime.now());
        sysMenuMapper.updateById(sysMenu);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteById(Long id) {
        Long count = sysMenuMapper.selectCount(
                new LambdaQueryWrapper<SysMenu>()
                        .eq(SysMenu::getParentId, id)
        );
        if (count > 0) {
            throw new BusinessException("请先删除子菜单");
        }
        // 清除所有该菜单相关的权限
        sysPermissionCacheManager.clearPermissionListByMenuId(id);
        //删除菜单和角色的中间表
        sysRoleMenuMapper.delete(
                new LambdaQueryWrapper<SysRoleMenu>()
                        .eq(SysRoleMenu::getMenuId,id)
        );
        // 删除菜单
        sysMenuMapper.deleteById(id);
    }
}
