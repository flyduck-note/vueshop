package com.flyduck.modules.app.vo.comment;

import com.flyduck.entity.AppComment;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AppCommentDetailsRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-21
 */
@Data
public class AppCommentDetailsRespVO extends AppComment implements Serializable {
}
