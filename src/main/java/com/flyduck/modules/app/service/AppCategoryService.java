package com.flyduck.modules.app.service;

import com.flyduck.modules.app.vo.category.AppCategoryDetailsRespVO;

public interface AppCategoryService {
    AppCategoryDetailsRespVO getCategoryDetailsById(Long id);
}
