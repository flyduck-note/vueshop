package com.flyduck.manager;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeWapPayResponse;
import com.flyduck.common.exception.BusinessException;
import com.flyduck.entity.AppOrder;
import com.flyduck.modules.app.properties.AlipayProperties;
import com.flyduck.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * <p>
 * AppPayManager
 * </p>
 *
 * @author flyduck
 * @since 2024-05-22
 */
@Slf4j
@Component
public class AppPayManager {

    @Resource
    private AlipayProperties alipayProperties;

    public AlipayTradeWapPayResponse alipay(AppOrder appOrder) {
        AlipayClient alipayClient = new DefaultAlipayClient(
                alipayProperties.getServerUrl(),
                alipayProperties.getAppId(),
                alipayProperties.getPrivateKey(),
                "json",
                "UTF-8",
                alipayProperties.getAlipayPublicKey(),
                "RSA2");

        AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
        request.setNotifyUrl(alipayProperties.getNotifyUrl());
        request.setReturnUrl(alipayProperties.getReturnUrl());
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", appOrder.getSn());
        bizContent.put("total_amount", appOrder.getTotalAmount());
        bizContent.put("subject", "VueShop商城购买商品");
        bizContent.put("product_code", "QUICK_WAP_WAY");
        bizContent.put("time_expire", DateUtils.formatLocalDateTime(appOrder.getCreated().plusHours(1)));

        request.setBizContent(bizContent.toString());
        AlipayTradeWapPayResponse response = null;
        try {
            log.info("支付宝支付接口入参：\n{}", JSONObject.toJSONString(bizContent, true));
            response = alipayClient.pageExecute(request);
            log.info("支付宝支付接口出参：\n{}", JSONObject.toJSONString(response, true));
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("支付异常，请稍后重试！",e);
        }
        return response;
    }

    public AlipayTradeQueryResponse aliCheck(String orderSn) {
        AlipayClient alipayClient = new DefaultAlipayClient(
                alipayProperties.getServerUrl(),
                alipayProperties.getAppId(),
                alipayProperties.getPrivateKey(),
                "json",
                "UTF-8",
                alipayProperties.getAlipayPublicKey(),
                "RSA2");
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", orderSn);
        request.setBizContent(bizContent.toString());
        AlipayTradeQueryResponse response = null;
        try {
            log.info("支付宝支付状态查询接口入参：\n{}", JSONObject.toJSONString(bizContent, true));
            response = alipayClient.execute(request);
            log.info("支付宝支付状态查询接口出参：\n{}", JSONObject.toJSONString(response, true));
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("查询异常，请稍后重试！",e);
        }
        return response;
    }

    public void refund(Long refundId) {
        //todo 退款
    }
}
