package com.flyduck.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 22:08
 **/
public class PageUtils {

    public static <S,T> Page<T> convert(Page<S> page,Class<T> t){
        Page<T> tPage = new Page<>();

        tPage.setTotal(page.getTotal());
        tPage.setSize(page.getSize());
        tPage.setCurrent(page.getCurrent());

        List<T> tList = BeanUtils.toBeanList(page.getRecords(), t);
        tPage.setRecords(tList);

        return tPage;

    }
}
