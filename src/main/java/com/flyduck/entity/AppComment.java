package com.flyduck.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
 *
 * @TableName app_comment
 */
@TableName(value ="app_comment")
@Data
public class AppComment implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     *
     */
    private Long userId;

    /**
     *
     */
    private Long productId;

    /**
     *
     */
    private Long orderItemId;

    /**
     *
     */
    private Long orderId;

    /**
     *
     */
    private String content;

    /**
     *
     */
    private Integer score;

    /**
     *
     */
    private String images;

    /**
     *
     */
    private String reply;

    /**
     *
     */
    private LocalDateTime created;

    /**
     *
     */
    private LocalDateTime updated;

    /**
     *
     */
    private Boolean isDelete;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
