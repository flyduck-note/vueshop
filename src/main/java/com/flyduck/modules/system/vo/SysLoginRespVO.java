package com.flyduck.modules.system.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * LoginResponse
 * </p>
 *
 * @author flyduck
 * @since 2024/5/9
 */
@Data
public class SysLoginRespVO implements Serializable {

    private String token;
}
