package com.flyduck.mapper;

import com.flyduck.entity.AppAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.flyduck.entity.AppAddress
 */
public interface AppAddressMapper extends BaseMapper<AppAddress> {

}




