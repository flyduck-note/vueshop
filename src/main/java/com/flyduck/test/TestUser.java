package com.flyduck.test;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户表
 * @TableName app_user
 */
@Data
public class TestUser implements Serializable {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    @NotBlank(message = "昵称不能为空")
    private String username;

    /**
     *
     */
    @NotBlank(message = "密码不能为空")
    private String password;

    /**
     *
     */
    private String tel;

    /**
     *
     */
    private Date created;

    /**
     *
     */
    private Date updated;

    /**
     *
     */
    private String avatar;

    /**
     *
     */
    private Date lastLogin;

    /**
     *
     */
    private Boolean isDelete;

    private static final long serialVersionUID = 1L;
}
