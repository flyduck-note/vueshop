package com.flyduck.modules.app.controller;

import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.vo.login.AppCaptchaRespVO;
import com.flyduck.modules.app.vo.login.AppLoginReqVO;
import com.flyduck.modules.app.vo.login.AppLoginRespVO;
import com.flyduck.modules.app.vo.login.AppRegisterReqVO;
import com.flyduck.modules.app.service.AppLoginService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * LoginController
 * </p>
 *
 * @author flyduck
 * @since 2024/5/9
 */
@RestController
@RequestMapping("/app")
public class AppLoginController {

    @Resource
    private AppLoginService appLoginService;

    @PostMapping("/login")
    public Result<AppLoginRespVO> login(@Validated @RequestBody AppLoginReqVO appLoginReqVO){
        AppLoginRespVO appLoginRespVO = appLoginService.login(appLoginReqVO);
        return Result.success(appLoginRespVO);
    }

    @GetMapping("/getCaptcha")
    public Result<AppCaptchaRespVO> getCaptcha(){
        AppCaptchaRespVO appCaptchaRespVO = appLoginService.getCaptcha();
        return Result.success(appCaptchaRespVO);
    }

    @PostMapping("/register")
    public Result<Void> register(@Validated @RequestBody AppRegisterReqVO appRegisterReqVO){
        appLoginService.register(appRegisterReqVO);
        return Result.success();
    }
}
