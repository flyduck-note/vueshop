package com.flyduck.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.config.BaseMapperX;
import com.flyduck.entity.AppOrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @Entity com.flyduck.entity.AppOrderItem
 */
public interface AppOrderItemMapper extends BaseMapperX<AppOrderItem> {

    default List<AppOrderItem> getAppOrderItemListByOrderId(Long orderId){
        return this.selectList(
                new LambdaQueryWrapper<AppOrderItem>()
                .eq(AppOrderItem::getOrderId,orderId)
        );
    }
}




