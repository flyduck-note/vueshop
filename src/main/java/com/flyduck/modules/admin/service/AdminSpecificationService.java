package com.flyduck.modules.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.admin.vo.*;

import java.util.List;

/**
 * <p>
 * AdminSpecificationService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-05
 */
public interface AdminSpecificationService {
    Page<AdminSpecificationRespVO> getSpecificationPage(Page page, AdminSpecificationQueryReqVO adminSpecificationQueryReqVO);

    AdminSpecificationDetailsRespVO getSpecificationDetailsById(Long id);

    void saveSpecification(AdminSpecificationSaveReqVO adminSpecificationSaveReqVO);

    void updateSpecification(AdminSpecificationUpdateReqVO adminSpecificationUpdateReqVO);

    void batchDeleteByIds(List<Long> ids);
}
