package com.flyduck.mapper;

import com.flyduck.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity com.flyduck.entity.SysMenu
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {
    List<SysMenu> getEnableMenuListByUserId(@Param("userId") Long userId);
}




