package com.flyduck.modules.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.admin.service.AdminOrderService;
import com.flyduck.modules.admin.service.AdminRefundService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.modules.app.controller.BaseController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * AdminOrderController
 * </p>
 *
 * @author flyduck
 * @since 2024-06-11
 */
@RestController
@RequestMapping("/admin/refund")
public class AdminRefundController extends BaseController {

    @Resource
    private AdminRefundService adminRefundService;

    @GetMapping("/getRefundPage")
    public Result<Page<AdminRefundRespVO>> getRefundPage(AdminRefundQueryReqVO adminRefundQueryReqVO){
        Page<AdminRefundRespVO> adminRefundRespVOPage = adminRefundService.getRefundPage(getPage(), adminRefundQueryReqVO);
        return Result.success(adminRefundRespVOPage);
    }

    @GetMapping("/getRefundStatusList")
    public Result<List<AdminDictRespVO>> getRefundStatusList(){
        List<AdminDictRespVO> adminDictRespVOList = adminRefundService.getRefundStatusList();
        return Result.success(adminDictRespVOList);
    }

    @GetMapping("/getRefundDetailsById/{id}")
    public Result<AdminRefundDetailsRespVO> getRefundDetailsById(@PathVariable("id") Long id){
        AdminRefundDetailsRespVO adminRefundDetailsRespVO = adminRefundService.getRefundDetailsById(id);
        return Result.success(adminRefundDetailsRespVO);
    }

    @PostMapping("/auditRefund")
    public Result<Void> auditRefund(@RequestBody AdminRefundAuditReqVO adminRefundAuditReqVO){
        adminRefundService.auditRefund(adminRefundAuditReqVO);
        return Result.success();
    }
}
