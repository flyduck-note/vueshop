package com.flyduck.modules.app.vo.cartItem;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * AppCartItemAddRequest
 * </p>
 *
 * @author flyduck
 * @since 2024-05-20
 */
@Data
public class AppCartItemAddReqVO {
    /**
     *
     */
    @NotNull(message = "请选择商品规格")
    private Long skuId;

    /**
     *
     */
    @NotNull(message = "请选择商品")
    private Long productId;

    /**
     *
     */
    @NotNull(message = "请选择购买数量")
    private Integer quantity;
}
