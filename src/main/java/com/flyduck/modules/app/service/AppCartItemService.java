package com.flyduck.modules.app.service;

import com.flyduck.modules.app.vo.cartItem.AppCartItemAddReqVO;
import com.flyduck.modules.app.vo.cartItem.AppCartItemRespVO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public interface AppCartItemService {
    List<AppCartItemRespVO> getCartItemList();

    Long getTotalCount();

    BigDecimal getTotalAmountByIds(ArrayList<Long> ids);

    void updateQuantityById(Long cartItemId, Integer quantity);

    void addCartItem(AppCartItemAddReqVO appCartItemAddReqVO);

    void batchDeleteByIds(Long[] ids);
}
