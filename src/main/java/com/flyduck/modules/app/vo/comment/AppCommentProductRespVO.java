package com.flyduck.modules.app.vo.comment;

import com.flyduck.entity.AppOrderItem;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-23 22:01
 **/
@Data
public class AppCommentProductRespVO extends AppOrderItem implements Serializable {
}
