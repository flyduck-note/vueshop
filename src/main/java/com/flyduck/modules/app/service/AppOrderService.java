package com.flyduck.modules.app.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.app.vo.delivery.AppDeliveryInfoRespVO;
import com.flyduck.modules.app.vo.order.*;

public interface AppOrderService {
    AppOrderPreviewRespVO previewOrder(AppOrderPreviewReqVO appOrderPreviewReqVO);

    String createOrder(AppOrderCreateReqVO appOrderCreateReqVO);

    AppOrderTotalCountRespVO getOrderTotalCount();

    Page<AppOrderRespVO> getOrderPageByStatus(Page page, Integer status);

    AppOrderDetailsRespVO getOrderDetailsById(Long orderId);

    void cancelOrderById(Long orderId);

    void deleteById(Long orderId);

    AppDeliveryInfoRespVO getDeliveryInfoById(Long orderId) throws Exception;

    void confirmOrderById(Long orderId);
}
