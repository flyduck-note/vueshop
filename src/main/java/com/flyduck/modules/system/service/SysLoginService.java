package com.flyduck.modules.system.service;

import com.flyduck.modules.system.vo.SysLoginReqVO;
import com.flyduck.modules.system.vo.SysLoginRespVO;
import com.flyduck.modules.system.vo.SysUserInfoRespVO;

public interface SysLoginService {
    SysUserInfoRespVO getUserInfo();

}
