package com.flyduck.modules.system.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * SysRoleReqVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysUserSaveReqVO implements Serializable {

    /**
     *
     */
    @NotBlank(message = "用户名不能为空")
    private String username;

    private String password;

    /**
     *
     */
    private String avatar;

    /**
     *
     */
    @NotBlank(message = "邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    private String email;

    /**
     *
     */
    private String phone;
    /**
     *
     */
    @NotNull(message = "状态不能为空")
    private Integer status;
}
