package com.flyduck.modules.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.admin.vo.*;

import java.util.List;

/**
 * <p>
 * AdminOrderService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-11
 */
public interface AdminOrderService {

    Page<AdminOrderRespVO> getOrderPage(Page page, AdminOrderQueryReqVO adminOrderQueryReqVO);

    List<AdminDictRespVO> getOrderStatusList();

    AdminOrderDetailsRespVO getOrderDetailsById(Long id);

    void orderDelivery(AdminOrderDeliveryReqVO adminOrderDeliveryReqVO);

    List<AdminDeliveryTraceRespVO> getDeliveryTraceListById(Long id) throws Exception;

    void closeOrder(AdminOrderCloseReqVO adminOrderDeliveryReqVO);
}
