package com.flyduck.modules.admin.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AdminOrderCloseReqVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-13
 */
@Data
public class AdminOrderCloseReqVO implements Serializable {
    private Long id;
    private String adminNote;
}
