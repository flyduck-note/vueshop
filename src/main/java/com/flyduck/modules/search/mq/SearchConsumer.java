package com.flyduck.modules.search.mq;

import com.flyduck.entity.AppProduct;
import com.flyduck.mapper.AppProductMapper;
import com.flyduck.modules.search.config.SearchRabbitConfig;
import com.flyduck.modules.search.document.EsProductDoc;
import com.flyduck.modules.search.repository.EsProductDocRepository;
import com.flyduck.utils.BeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-25 07:56
 **/
@Slf4j
@Component
public class SearchConsumer {

    @Resource
    private AppProductMapper appProductMapper;
    @Resource
    private EsProductDocRepository esProductDocRepository;

    @RabbitListener(queues = SearchRabbitConfig.PRODUCT_SEARCH_CREATE_QUEUE)
    public void createListener(Long productId){
        AppProduct appProduct = appProductMapper.selectById(productId);
        //查询不到，或者下架了，需要从es删除
        if(appProduct == null || !appProduct.getIsOnSale()){
            esProductDocRepository.deleteById(productId);
            log.info("【监听到商品创建】从es删除商品成功,productId={}", productId);
        }else {
            EsProductDoc esProductDoc = BeanUtils.toBean(appProduct, EsProductDoc.class);
            esProductDocRepository.save(esProductDoc);
            log.info("【监听到商品创建】保存商品到es成功,productId={}", productId);
        }
    }

    @RabbitListener(queues = SearchRabbitConfig.PRODUCT_SEARCH_DELETE_QUEUE)
    public void deleteListener(List<Long> productIds){
        esProductDocRepository.deleteAllById(productIds);
        log.info("【监听到商品删除】从es删除商品成功,productIds={}", productIds);
    }
}
