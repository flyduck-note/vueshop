package com.flyduck.modules.app.mq;

import com.flyduck.modules.app.config.RabbitConfig;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * <p>
 * MqSender
 * </p>
 *
 * @author flyduck
 * @since 2024-05-21
 */
@Component
public class MqSender {

    @Resource
    private AmqpTemplate amqpTemplate;

    public void sendCancelOrderMessage(long orderId){
        amqpTemplate.convertAndSend(RabbitConfig.ORDER_CANCEL_EXCHANGE,
                RabbitConfig.ORDER_CANCEL_ROUTE_KEY,
                orderId,
                //30分钟后消息过期
                message -> {
                    message.getMessageProperties().setExpiration(String.valueOf(30 * 60 * 1000));
                    return message;
                }
        );
    }
}
