package com.flyduck.modules.app.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.app.vo.product.AppProductQueryReqVO;
import com.flyduck.modules.app.vo.product.AppProductRespVO;
import com.flyduck.modules.app.vo.product.AppProductDetailsRespVO;

public interface AppProductService {
    Page<AppProductRespVO> getProductPage(Page page, AppProductQueryReqVO appProductQueryReqVO);

    AppProductDetailsRespVO getProductDetailsById(Long productId);
}
