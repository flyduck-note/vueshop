package com.flyduck.mapper;

import com.flyduck.config.BaseMapperX;
import com.flyduck.entity.AppSkuStock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity com.flyduck.entity.AppSkuStock
 */
public interface AppSkuStockMapper extends BaseMapperX<AppSkuStock> {

    int reduceStock(@Param("skuId") Long skuId, @Param("quantity") Integer quantity);

    int releaseStock(@Param("skuId") Long skuId, @Param("quantity") Integer quantity);
}




