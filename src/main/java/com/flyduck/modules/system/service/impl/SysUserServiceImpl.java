package com.flyduck.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.entity.SysRole;
import com.flyduck.entity.SysUser;
import com.flyduck.entity.SysUserRole;
import com.flyduck.mapper.SysRoleMapper;
import com.flyduck.mapper.SysUserMapper;
import com.flyduck.mapper.SysUserRoleMapper;
import com.flyduck.modules.system.constant.SysConstant;
import com.flyduck.manager.SysPermissionCacheManager;
import com.flyduck.modules.system.service.SysUserService;
import com.flyduck.modules.system.vo.*;
import com.flyduck.utils.BeanUtils;
import com.flyduck.utils.PageUtils;
import com.flyduck.utils.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * SysUserServiceImpl
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private SysRoleMapper sysRoleMapper;
    @Resource
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Resource
    private SysUserRoleMapper sysUserRoleMapper;
    @Resource
    private SysPermissionCacheManager sysPermissionCacheManager;

    @Override
    public Page<SysUserRespVO> getUserPage(Page page, SysUserQueryReqVO sysUserQueryReqVO) {
        Page<SysUser> sysUserPage = sysUserMapper.selectPage(page,
                new LambdaQueryWrapper<SysUser>()
                        .like(StringUtils.isNotBlank(sysUserQueryReqVO.getUsername()), SysUser::getUsername, sysUserQueryReqVO.getUsername())
                        .orderByDesc(SysUser::getCreated)
        );

        Page<SysUserRespVO> sysUserRespVOPage = PageUtils.convert(sysUserPage, SysUserRespVO.class);
        sysUserRespVOPage.getRecords().stream().forEach(sysUserRespVO -> {
            List<SysRole> sysRoleList = sysRoleMapper.getRoleListByUserId(sysUserRespVO.getId());
            List<SysUserRespVO.SysRoleDTO> sysRoleDTOS = BeanUtils.toBeanList(sysRoleList, SysUserRespVO.SysRoleDTO.class);
            sysUserRespVO.setRoles(sysRoleDTOS);
        });

        return sysUserRespVOPage;
    }

    @Override
    public SysUserDetailsRespVO getUserDetailsById(Long id) {
        SysUser sysUser = sysUserMapper.selectById(id);
        SysUserDetailsRespVO sysUserDetailsRespVO = BeanUtils.toBean(sysUser, SysUserDetailsRespVO.class);

        List<SysRole> sysRoleList = sysRoleMapper.getRoleListByUserId(id);
        List<SysUserDetailsRespVO.SysRoleDTO> sysRoleDTOS = BeanUtils.toBeanList(sysRoleList, SysUserDetailsRespVO.SysRoleDTO.class);
        sysUserDetailsRespVO.setRoles(sysRoleDTOS);

        return sysUserDetailsRespVO;
    }

    @Override
    public void saveUser(SysUserSaveReqVO sysUserSaveReqVO) {
        SysUser sysUser = BeanUtils.toBean(sysUserSaveReqVO, SysUser.class);
        sysUser.setCreated(LocalDateTime.now());
        //默认密码
        if (StringUtils.isBlank(sysUserSaveReqVO.getPassword())) {
            sysUser.setPassword(bCryptPasswordEncoder.encode(SysConstant.DEFAULT_PASSWORD));
        }else {
            sysUser.setPassword(bCryptPasswordEncoder.encode(sysUserSaveReqVO.getPassword()));
        }
        //默认头像
        sysUser.setAvatar(SysConstant.DEFAULT_AVATAR);
        sysUserMapper.insert(sysUser);
    }

    @Override
    public void updateUser(SysUserUpdateReqVO sysUserUpdateReqVO) {
        SysUser updateSysUser = BeanUtils.toBean(sysUserUpdateReqVO, SysUser.class);//页面的数据
        SysUser sysUser = sysUserMapper.selectById(sysUserUpdateReqVO.getId());//数据库的数据
        BeanUtils.copyProperties(updateSysUser, sysUser, "password","created","updated");//将页面的数据 设置到 数据库的数据 里面去（忽略password，created，updated）
        //页面传递密码不为空，才设置密码
        if(StringUtils.isNotBlank(sysUserUpdateReqVO.getPassword())){
            sysUser.setPassword(bCryptPasswordEncoder.encode(sysUserUpdateReqVO.getPassword()));
        }
        sysUser.setUpdated(LocalDateTime.now());
        sysUserMapper.updateById(sysUser);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchDeleteByIds(List<Long> ids) {
        //先删除用户角色管理数据
        sysUserRoleMapper.delete(
                new LambdaQueryWrapper<SysUserRole>()
                        .in(SysUserRole::getUserId,ids)
        );

        //删除用户的权限缓存
        ids.forEach(id -> {
            sysPermissionCacheManager.clearPermissionListByUserId(id);
        });

        sysUserMapper.deleteBatchIds(ids);


    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void assignRolesToUser(Long userId, List<Long> roleIds) {
        //删除之前的关系
        sysUserRoleMapper.delete(
                new LambdaQueryWrapper<SysUserRole>()
                        .eq(SysUserRole::getUserId,userId)
        );

        //建立新的关系
        List<SysUserRole> sysUserRoleList = roleIds.stream()
                .map(roleId -> {
                    SysUserRole sysUserRole = new SysUserRole();
                    sysUserRole.setUserId(userId);
                    sysUserRole.setRoleId(roleId);
                    return sysUserRole;
                }).collect(Collectors.toList());

        sysUserRoleMapper.insertBatch(sysUserRoleList);

        //删除缓存
        sysPermissionCacheManager.clearPermissionListByUserId(userId);
    }

    @Override
    public void resetPasswordById(Long userId) {
        SysUser sysUser = sysUserMapper.selectById(userId);
        sysUser.setPassword(bCryptPasswordEncoder.encode(SysConstant.DEFAULT_PASSWORD));
        sysUser.setUpdated(LocalDateTime.now());
        sysUserMapper.updateById(sysUser);
    }
}
