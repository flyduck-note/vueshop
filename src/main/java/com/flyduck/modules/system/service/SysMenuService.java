package com.flyduck.modules.system.service;

import com.flyduck.modules.system.vo.SysMenuDetailsRespVO;
import com.flyduck.modules.system.vo.SysMenuSaveReqVO;
import com.flyduck.modules.system.vo.SysMenuTreeRespVO;
import com.flyduck.modules.system.vo.SysMenuUpdateReqVO;

import java.util.List;

/**
 * <p>
 * SysMenuService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
public interface SysMenuService {
    List<SysMenuTreeRespVO> getMenuTree();

    SysMenuDetailsRespVO getMenuDetailsById(Long id);

    void saveMenu(SysMenuSaveReqVO sysMenuSaveReqVO);

    void updateMenu(SysMenuUpdateReqVO sysMenuUpdateReqVO);

    void deleteById(Long id);
}
