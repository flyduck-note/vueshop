package com.flyduck.modules.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.entity.AppProduct;
import com.flyduck.entity.AppSkuStock;
import com.flyduck.entity.AppSpecificationValue;
import com.flyduck.manager.GenerateSnManager;
import com.flyduck.mapper.AppProductMapper;
import com.flyduck.modules.admin.service.AdminProductService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.modules.search.config.SearchRabbitConfig;
import com.flyduck.utils.BeanUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import com.flyduck.manager.AppSpecificationManager;
import com.flyduck.manager.AppSkuStockManager;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * AdminProductServiceImpl
 * </p>
 *
 * @author flyduck
 * @since 2024-06-05
 */
@Service
public class AdminProductServiceImpl implements AdminProductService {

    @Resource
    private AppProductMapper appProductMapper;
    @Resource
    private AppSpecificationManager appSpecificationManager;
    @Resource
    private AppSkuStockManager appSkuStockManager;
    @Resource
    private GenerateSnManager generateSnManager;
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Override
    public Page<AdminProductRespVO> getProductPage(Page page, AdminProductQueryReqVO adminProductQueryReqVO) {
        return appProductMapper.getProductPage(page,adminProductQueryReqVO);
    }

    @Override
    public AdminProductDetailsRespVO getProductDetailsById(Long id) {
        // 商品详情
        AppProduct appProduct = appProductMapper.selectById(id);

        AdminProductDetailsRespVO adminProductDetailsRespVO = BeanUtils.toBean(appProduct, AdminProductDetailsRespVO.class);

        // 商品属性
        List<AppSpecificationValue> specificationValueList = appSpecificationManager.getAppSpecificationValueListByProductId(id);
        List<AdminProductDetailsRespVO.AdminSpecificationValueVO> adminSpecificationValueVOList = BeanUtils.toBeanList(specificationValueList, AdminProductDetailsRespVO.AdminSpecificationValueVO.class);

        // 商品可选sku及库存
        List<AppSkuStock> appSkuStockList = appSkuStockManager.getAppSkuStockListByProductId(id);
        List<AdminProductDetailsRespVO.AdminSkuStockVO> adminSkuStockVOList = BeanUtils.toBeanList(appSkuStockList, AdminProductDetailsRespVO.AdminSkuStockVO.class);

        adminProductDetailsRespVO.setSpecifications(adminSpecificationValueVOList);
        adminProductDetailsRespVO.setSkuStocks(adminSkuStockVOList);
        return adminProductDetailsRespVO;
    }

    @Override
    public void saveProduct(AdminProductSaveReqVO adminProductSaveReqVO) {
        AppProduct appProduct = BeanUtils.toBean(adminProductSaveReqVO, AppProduct.class);
        appProduct.setCreated(LocalDateTime.now());
        appProduct.setSn(generateSnManager.generateProductSn());
        appProductMapper.insert(appProduct);

        //同步es和mysql的商品数据
        rabbitTemplate.convertAndSend(SearchRabbitConfig.PRODUCT_SEARCH_EXCHANGE, SearchRabbitConfig.PRODUCT_SEARCH_CREATE_QUEUE, appProduct.getId());
    }

    @Override
    public void updateProduct(AdminProductUpdateReqVO adminProductUpdateReqVO) {
        AppProduct appProduct = BeanUtils.toBean(adminProductUpdateReqVO, AppProduct.class);
        appProduct.setUpdated(LocalDateTime.now());
        appProductMapper.updateById(appProduct);
        //同步es和mysql的商品数据
        rabbitTemplate.convertAndSend(SearchRabbitConfig.PRODUCT_SEARCH_EXCHANGE, SearchRabbitConfig.PRODUCT_SEARCH_CREATE_QUEUE, appProduct.getId());
    }

    @Override
    public void batchDeleteByIds(List<Long> ids) {
        appProductMapper.deleteBatchIds(ids);
        //同步es和mysql的商品数据
        rabbitTemplate.convertAndSend(SearchRabbitConfig.PRODUCT_SEARCH_EXCHANGE, SearchRabbitConfig.PRODUCT_SEARCH_DELETE_KEY, ids);

    }

    @Override
    public void updateStatusById(Long id, AdminProductUpdateStatusReqVO adminProductUpdateStatusReqVO) {
        appProductMapper.update(
                new LambdaUpdateWrapper<AppProduct>()
                        .set(adminProductUpdateStatusReqVO.getIsOnSale() != null, AppProduct::getIsOnSale, adminProductUpdateStatusReqVO.getIsOnSale())
                        .set(adminProductUpdateStatusReqVO.getIsNew() != null, AppProduct::getIsNew, adminProductUpdateStatusReqVO.getIsNew())
                        .set(adminProductUpdateStatusReqVO.getIsTop() != null, AppProduct::getIsTop, adminProductUpdateStatusReqVO.getIsTop())
                        .set(adminProductUpdateStatusReqVO.getIsHot() != null, AppProduct::getIsHot, adminProductUpdateStatusReqVO.getIsHot())
                        .eq(AppProduct::getId, id)
        );

        //上架的商品才显示，所以要同步到es里面
        if(adminProductUpdateStatusReqVO.getIsOnSale() != null){
            rabbitTemplate.convertAndSend(SearchRabbitConfig.PRODUCT_SEARCH_EXCHANGE, SearchRabbitConfig.PRODUCT_SEARCH_CREATE_QUEUE, id);
        }
    }
}
