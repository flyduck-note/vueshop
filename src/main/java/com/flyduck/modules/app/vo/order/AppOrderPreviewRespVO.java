package com.flyduck.modules.app.vo.order;

import com.flyduck.entity.AppAddress;
import com.flyduck.entity.AppCartItem;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-20 20:47
 **/
@Data
public class AppOrderPreviewRespVO implements Serializable {

    private AppAddressVO address;
    private List<AppCartItemVO> cartItems;
    private BigDecimal totalAmount;
    private BigDecimal freight;

    @Data
    public static class AppAddressVO extends AppAddress implements Serializable{
        private String address;

        public String getAddress() {
            return super.getProvince() + super.getCity() + super.getCounty() + super.getAddressDetail();
        }
    }

    @Data
    public static class AppCartItemVO extends AppCartItem implements Serializable{
        private String productName;
        private String productImage;

        private String sku;
        private BigDecimal price;
    }
}
