package com.flyduck.modules.admin.service;

import com.flyduck.modules.admin.vo.AdminSkuDetailsRespVO;
import com.flyduck.modules.admin.vo.AdminSkuSaveReqVO;

import java.util.List;

/**
 * <p>
 * AdminSkuService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-06
 */
public interface AdminSkuService {
    AdminSkuDetailsRespVO getSkuDetailsByProductId(Long productId);

    void saveSku(AdminSkuSaveReqVO adminSkuSaveReqVO);
}
