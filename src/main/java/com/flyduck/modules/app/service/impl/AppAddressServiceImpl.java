package com.flyduck.modules.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.flyduck.common.exception.BusinessException;
import com.flyduck.entity.AppAd;
import com.flyduck.entity.AppAddress;
import com.flyduck.mapper.AppAddressMapper;
import com.flyduck.modules.app.service.AppAddressService;
import com.flyduck.modules.app.vo.address.AppAddressDetailsRespVO;
import com.flyduck.modules.app.vo.address.AppAddressRespVO;
import com.flyduck.modules.app.vo.address.AppAddressSaveReqVO;
import com.flyduck.modules.app.vo.address.AppAddressUpdateReqVO;
import com.flyduck.utils.BeanUtils;
import com.flyduck.utils.TokenUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * AppAddressServiceImpl
 * </p>
 *
 * @author flyduck
 * @since 2024-06-19
 */
@Service
public class AppAddressServiceImpl implements AppAddressService {

    @Resource
    private AppAddressMapper appAddressMapper;
    @Resource
    private TokenUtils tokenUtils;

    @Override
    public List<AppAddressRespVO> getAddressList() {

        List<AppAddress> appAddressList = appAddressMapper.selectList(
                new LambdaQueryWrapper<AppAddress>()
                        .eq(AppAddress::getUserId, tokenUtils.getCurrentUserId())
                        .orderByDesc(AppAddress::getCreated)
        );
        return BeanUtils.toBeanList(appAddressList,AppAddressRespVO.class);

    }

    @Override
    public AppAddressDetailsRespVO getAddressDetailsById(Long id) {
        AppAddress appAddress = appAddressMapper.selectById(id);
        if(appAddress == null || !appAddress.getUserId().equals(tokenUtils.getCurrentUserId()) ){
            throw new BusinessException("收货地址不存在");
        }
        return BeanUtils.toBean(appAddress,AppAddressDetailsRespVO.class);
    }

    @Override
    public void saveAddress(AppAddressSaveReqVO appAddressSaveReqVO) {
        AppAddress appAddress = BeanUtils.toBean(appAddressSaveReqVO, AppAddress.class);
        appAddress.setCreated(LocalDateTime.now());
        appAddress.setUserId(tokenUtils.getCurrentUserId());
        // 如果是默认地址，则把以前的默认地址改为非默认地址
        if (appAddressSaveReqVO.getIsDefault()) {
            appAddressMapper.update(
                new LambdaUpdateWrapper<AppAddress>()
                    .set(AppAddress::getIsDefault, false )
                    .eq(AppAddress::getUserId,tokenUtils.getCurrentUserId())
                    .eq(AppAddress::getIsDefault,true)
            );
        }
        appAddressMapper.insert(appAddress);
    }

    @Override
    public void updateAddress(AppAddressUpdateReqVO appAddressUpdateReqVO) {
        AppAddress appAddress = BeanUtils.toBean(appAddressUpdateReqVO, AppAddress.class);
        //校验
        Long count = appAddressMapper.selectCount(
                new LambdaQueryWrapper<AppAddress>()
                        .eq(AppAddress::getUserId, tokenUtils.getCurrentUserId())
                        .eq(AppAddress::getId, appAddressUpdateReqVO.getId())
        );
        if(count <= 0){
            throw new BusinessException("收货地址不存在");
        }

        // 如果是默认地址，则把以前的默认地址改为非默认地址
        if (appAddressUpdateReqVO.getIsDefault()) {
            appAddressMapper.update(
                    new LambdaUpdateWrapper<AppAddress>()
                            .set(AppAddress::getIsDefault, false )
                            .eq(AppAddress::getUserId,tokenUtils.getCurrentUserId())
                            .eq(AppAddress::getIsDefault,true)
            );
        }

        appAddress.setUpdated(LocalDateTime.now());
        appAddressMapper.updateById(appAddress);
    }

    @Override
    public void deleteById(Long id) {
        appAddressMapper.delete(
                new LambdaQueryWrapper<AppAddress>()
                .eq(AppAddress::getUserId,tokenUtils.getCurrentUserId())
                .eq(AppAddress::getId,id)
        );
    }
}
