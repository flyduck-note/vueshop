package com.flyduck.modules.app.vo.home;

import com.flyduck.entity.AppAd;
import com.flyduck.entity.AppCategory;
import com.flyduck.entity.AppProduct;
import lombok.Data;

import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 20:06
 **/
@Data
public class AppHomeDataRespVO {

    /**
     * 轮播图
     */
    private List<AppAdVO> carousels;
    /**
     * 分类数据
     */
    private List<AppCategoryVO> categories;
    /**
     * 商品列表数据
     */
    private List<AppProductVO> products;

    /**
     * 轮播图
     */
    @Data
    public static class AppAdVO extends AppAd {

    }

    /**
     * 分类数据
     */
    @Data
    public static class AppCategoryVO extends AppCategory {

    }

    /**
     * 商品列表数据
     */
    @Data
    public static class AppProductVO extends AppProduct {

    }
}
