package com.flyduck.modules.search.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.search.service.AppSearchService;
import com.flyduck.modules.search.vo.AppEsProductRespVO;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * AppSearchController
 * </p>
 *
 * @author flyduck
 * @since 2024-05-24
 */
@RestController
@RequestMapping("/app/search")
public class AppSearchController {

    @Resource
    private AppSearchService appSearchService;

    @GetMapping("/getEsProductPage")
    public Result<Page<AppEsProductRespVO>> getEsProductPage(String keyword, HttpServletRequest request){
        int current = ServletRequestUtils.getIntParameter(request, "current",1);
        int size = ServletRequestUtils.getIntParameter(request, "size",10);

        Page<AppEsProductRespVO> appEsProductRespVOPage = appSearchService.getEsProductPage(keyword,current,size);
        return Result.success(appEsProductRespVOPage);
    }
}
