package com.flyduck.modules.admin.vo;

import com.flyduck.entity.AppProduct;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:48
 **/
@Data
public class AdminProductSaveReqVO implements Serializable {

    /**
     *
     */
    @NotBlank(message = "名称不能为空")
    private String name;

    /**
     *
     */
    @NotNull(message = "分类不能为空")
    private Long categoryId;

    /**
     *
     */
    @NotBlank(message = "照片不能为空")
    private String image;

    /**
     *
     */
    @NotBlank(message = "照片不能为空")
    private String pics;

    /**
     *
     */
    @NotNull(message = "价格不能为空")
    private BigDecimal price;

    /**
     *
     */
    @NotNull(message = "cost不能为空")
    private BigDecimal cost;

    /**
     * 库存
     */
    @NotNull(message = "库存不能为空")
    private Integer stock;

    /**
     *
     */
    @NotBlank(message = "关键字不能为空")
    private String keywords;

    /**
     *
     */
    @NotBlank(message = "商品详情不能为空")
    private String detail;

    /**
     *
     */
    @NotNull(message = "上架状态不能为空")
    private Boolean isOnSale;

    /**
     *
     */
    @NotNull(message = "上新状态不能为空")
    private Boolean isNew;

    /**
     *
     */
    @NotNull(message = "置顶状态不能为空")
    private Boolean isTop;

    /**
     *
     */
    @NotNull(message = "热门状态不能为空")
    private Boolean isHot;

    /**
     *
     */
    @NotNull(message = "排序不能为空")
    private Integer sortOrder;
}
