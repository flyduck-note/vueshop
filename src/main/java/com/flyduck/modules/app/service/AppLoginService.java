package com.flyduck.modules.app.service;

import com.flyduck.modules.app.vo.login.AppCaptchaRespVO;
import com.flyduck.modules.app.vo.login.AppLoginReqVO;
import com.flyduck.modules.app.vo.login.AppLoginRespVO;
import com.flyduck.modules.app.vo.login.AppRegisterReqVO;

/**
 * <p>
 * LoginService
 * </p>
 *
 * @author flyduck
 * @since 2024/5/9
 */
public interface AppLoginService {
    AppLoginRespVO login(AppLoginReqVO appLoginReqVO);

    AppCaptchaRespVO getCaptcha();

    void register(AppRegisterReqVO appRegisterReqVO);
}
