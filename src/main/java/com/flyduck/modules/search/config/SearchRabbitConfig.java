package com.flyduck.modules.search.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-25 07:34
 **/
@Configuration
public class SearchRabbitConfig {
    public final static String PRODUCT_SEARCH_EXCHANGE = "product-search-exchange";

    @Bean
    public TopicExchange productSearchExchange(){
        return new TopicExchange(PRODUCT_SEARCH_EXCHANGE,true,false);
    }





    public final static String PRODUCT_SEARCH_CREATE_QUEUE = "product-search-create-queue";
    public final static String PRODUCT_SEARCH_CREATE_KEY = "product.search.create";

    @Bean
    public Queue productSearchCreateQueue(){
        return QueueBuilder.durable(PRODUCT_SEARCH_CREATE_QUEUE)
                .build();
    }

    @Bean
    public Binding productSearchCreateBinding(){
        return BindingBuilder
                .bind(productSearchCreateQueue())
                .to(productSearchExchange())
                .with(PRODUCT_SEARCH_CREATE_KEY);
    }







    public final static String PRODUCT_SEARCH_DELETE_QUEUE = "product-search-delete-queue";
    public final static String PRODUCT_SEARCH_DELETE_KEY = "product.delete.create";

    @Bean
    public Queue productSearchDeleteQueue(){
        return QueueBuilder.durable(PRODUCT_SEARCH_DELETE_QUEUE)
                .build();
    }

    @Bean
    public Binding productSearchDeleteBinding(){
        return BindingBuilder
                .bind(productSearchDeleteQueue())
                .to(productSearchExchange())
                .with(PRODUCT_SEARCH_DELETE_KEY);
    }
}
