package com.flyduck.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.entity.AppCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @Entity com.flyduck.entity.AppCategory
 */
public interface AppCategoryMapper extends BaseMapper<AppCategory> {

}




