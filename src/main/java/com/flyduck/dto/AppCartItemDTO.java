package com.flyduck.dto;

import com.flyduck.entity.AppCartItem;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * AppCartItemDTO
 * </p>
 *
 * @author flyduck
 * @since 2024-05-21
 */
@Data
public class AppCartItemDTO extends AppCartItem implements Serializable {

    private String productName;
    private String productImage;
    private String productSn;
    private Long categoryId;

    private String sku;
    private BigDecimal price;

}
