package com.flyduck.mapper;

import com.flyduck.entity.AppSpecification;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.flyduck.entity.AppSpecification
 */
public interface AppSpecificationMapper extends BaseMapper<AppSpecification> {

}




