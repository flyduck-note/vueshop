package com.flyduck.modules.app.service;

import com.flyduck.modules.app.vo.home.AppHomeDataRespVO;

public interface AppHomeService {
    AppHomeDataRespVO getHomeData();
}
