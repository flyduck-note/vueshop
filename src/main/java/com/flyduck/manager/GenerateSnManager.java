package com.flyduck.manager;

import cn.hutool.core.util.RandomUtil;
import com.flyduck.utils.DateUtils;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * <p>
 * GenerateSnManager
 * </p>
 *
 * @author flyduck
 * @since 2024-06-06
 */
@Component
public class GenerateSnManager {
    /**
     * 生成订单的sn
     * @return
     */
    public String generateOrderSn() {
        String dateStr = DateUtils.format(new Date(), DateUtils.YYYYMMDDHHMMSS);
        return "D" + dateStr + RandomUtil.randomNumbers(4);
    }

    /**
     * 生成商品的sn
     * @return
     */
    public String generateProductSn() {
        String dateStr = DateUtils.format(new Date(), DateUtils.YYYYMMDDHHMMSS);
        return "PD" + dateStr + RandomUtil.randomNumbers(4);
    }
}
