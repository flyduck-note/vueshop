package com.flyduck.test;

import com.flyduck.common.lang.Result;
import com.flyduck.manager.AppUploadManager;
import com.flyduck.mapper.AppUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-06 21:51
 **/
@RestController
public class TestController {

    @Resource
    private AppUserMapper appUserMapper;
    @Autowired
    private AppUploadManager appUploadManager;

    @GetMapping("/test")
    public Object test(){
        return appUserMapper.selectList(null);
    }

    @PostMapping("/testSave")
    public Result testSave(@Validated @RequestBody TestUser testUser){
        return Result.success(testUser);
    }

    @PostMapping("/testUpload")
    public Result testUpload(MultipartFile[] files) throws IOException {
        return Result.success(appUploadManager.upload(files, 0.5F, 0.2));
    }
}
