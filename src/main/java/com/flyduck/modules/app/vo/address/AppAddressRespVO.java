package com.flyduck.modules.app.vo.address;

import com.flyduck.entity.AppAddress;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AppAdressRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-19
 */
@Data
public class AppAddressRespVO extends AppAddress implements Serializable {
}
