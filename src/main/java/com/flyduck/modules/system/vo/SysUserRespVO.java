package com.flyduck.modules.system.vo;

import com.flyduck.entity.SysRole;
import com.flyduck.entity.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * SysRoleRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysUserRespVO extends SysUser implements Serializable {
    private List<SysRoleDTO> roles;

    @Data
    public static class SysRoleDTO extends SysRole implements Serializable {

    }
}
