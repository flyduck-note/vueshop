package com.flyduck.modules.admin.controller;

import com.flyduck.common.lang.Result;
import com.flyduck.modules.admin.service.AdminSkuService;
import com.flyduck.modules.admin.vo.AdminSkuDetailsRespVO;
import com.flyduck.modules.admin.vo.AdminSkuSaveReqVO;
import com.flyduck.modules.app.controller.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * AdminSkuController
 * </p>
 *
 * @author flyduck
 * @since 2024-06-06
 */
@RestController
@RequestMapping("/admin/sku")
public class AdminSkuController extends BaseController {

    @Resource
    private AdminSkuService adminSkuService;

    @GetMapping("/getSkuDetailsByProductId/{productId}")
    public Result<AdminSkuDetailsRespVO> getSkuDetailsByProductId(@PathVariable("productId") Long productId){
        AdminSkuDetailsRespVO adminSkuDetailsRespVO = adminSkuService.getSkuDetailsByProductId(productId);
        return Result.success(adminSkuDetailsRespVO);
    }

    @PostMapping("/saveSku")
    public Result<Void> saveSku(@Validated @RequestBody AdminSkuSaveReqVO adminSkuSaveReqVO){
        adminSkuService.saveSku(adminSkuSaveReqVO);
        return Result.success();
    }
}

