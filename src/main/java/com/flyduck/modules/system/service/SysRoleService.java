package com.flyduck.modules.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.system.vo.*;

import java.util.List;

/**
 * <p>
 * SysRoleService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
public interface SysRoleService {
    Page<SysRoleRespVO> getRolePage(Page page, SysRoleQueryReqVO sysRoleQueryReqVO);

    SysRoleDetailsRespVO getRoleDetailsById(Long id);

    void saveRole(SysRoleSaveReqVO sysRoleSaveReqVO);

    void updateRole(SysRoleUpdateReqVO sysRoleUpdateReqVO);

    void batchDeleteByIds(List<Long> ids);

    void assignMenuListToRole(Long roleId, List<Long> menuIds);
}
