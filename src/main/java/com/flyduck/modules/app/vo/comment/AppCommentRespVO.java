package com.flyduck.modules.app.vo.comment;

import com.flyduck.entity.AppComment;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-23 21:42
 **/
@Data
public class AppCommentRespVO extends AppComment implements Serializable {

    private String username;
    private String userAvatar;
}
