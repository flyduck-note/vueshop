package com.flyduck.modules.admin.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:48
 **/
@Data
public class AdminCategorySaveReqVO implements Serializable {
    /**
     *
     */
    @NotBlank(message = "分类名称不能为空")
    private String name;

    /**
     *
     */
    @NotBlank(message = "图标不能为空")
    private String iconUrl;

    /**
     *
     */
    private String remark;

    /**
     *
     */
    private Boolean isShow;

    /**
     *
     */
    @NotNull(message = "排序不能为空")
    private Integer sortOrder;
}
