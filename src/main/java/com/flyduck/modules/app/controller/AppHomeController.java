package com.flyduck.modules.app.controller;

import com.flyduck.common.lang.Result;
import com.flyduck.modules.app.vo.home.AppHomeDataRespVO;
import com.flyduck.modules.app.service.AppHomeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 18:47
 **/
@RestController
@RequestMapping("/app/home")
public class AppHomeController {

    @Resource
    private AppHomeService appHomeService;

    @GetMapping("/getHomeData")
    public Result<AppHomeDataRespVO> getHomeData(){
        return Result.success(appHomeService.getHomeData());
    }
}
