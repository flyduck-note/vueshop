package com.flyduck.modules.app.vo.product;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-16 10:26
 **/
@Data
public class AppProductQueryReqVO implements Serializable {
    private Long categoryId;
    private Integer sort;
}
