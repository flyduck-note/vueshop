package com.flyduck.modules.admin.vo;

import com.flyduck.entity.AppProduct;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:48
 **/
@Data
public class AdminProductUpdateStatusReqVO implements Serializable {

    private Boolean isOnSale;
    private Boolean isNew;
    private Boolean isTop;
    private Boolean isHot;
}
