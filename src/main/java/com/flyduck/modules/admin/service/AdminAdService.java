package com.flyduck.modules.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.admin.vo.*;

import java.util.List;

public interface AdminAdService {
    Page<AdminAdRespVO> getAdPage(Page page, AdminAdQueryReqVO adminAdQueryReqVO);

    AdminAdDetailsRespVO getAdDetailsById(Long id);

    void saveAd(AdminAdSaveReqVO adminAdSaveReqVO);

    void updateAd(AdminAdUpdateReqVO adminAdUpdateReqVO);

    void batchDeleteByIds(List<Long> ids);
}
