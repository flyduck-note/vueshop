package com.flyduck.modules.search.config;

import com.flyduck.manager.AppSearchManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * <p>
 * SearchContextStartup
 * </p>
 *
 * @author flyduck
 * @since 2024-05-24
 */
@Slf4j
@Component
public class SearchContextStartup implements ApplicationRunner {

    @Resource
    private AppSearchManager appSearchManager;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        int count = appSearchManager.initEsData();
        log.info("已初始化es数据{}条",count);
    }
}
