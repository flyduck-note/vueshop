package com.flyduck.modules.search.vo;

import com.flyduck.modules.search.document.EsProductDoc;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AppEsProductRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-05-24
 */
@Data
public class AppEsProductRespVO extends EsProductDoc implements Serializable {
}
