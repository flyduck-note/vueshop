package com.flyduck.common.exception;

import lombok.Getter;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-07 21:10
 **/
@Getter
public class BusinessException extends RuntimeException {

    private int code = 400;
    private String message;

    public BusinessException(String message){
        super(message);
        this.message = message;
    }

    public BusinessException(String message, Exception e){
        super(message,e);
        this.message = message;
    }

    public BusinessException(int code,String message){
        super(message);
        this.code = code;
        this.message = message;
    }
}
