package com.flyduck.mapper;

import com.flyduck.config.BaseMapperX;
import com.flyduck.entity.AppSpecificationValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.flyduck.entity.AppSpecificationValue
 */
public interface AppSpecificationValueMapper extends BaseMapperX<AppSpecificationValue> {

}




