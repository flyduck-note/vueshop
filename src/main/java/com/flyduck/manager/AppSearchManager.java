package com.flyduck.manager;

import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.entity.AppProduct;
import com.flyduck.mapper.AppProductMapper;
import com.flyduck.modules.search.document.EsProductDoc;
import com.flyduck.modules.search.repository.EsProductDocRepository;
import com.flyduck.utils.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * AppSearchManager
 * </p>
 *
 * @author flyduck
 * @since 2024-05-24
 */
@Component
public class AppSearchManager {

    @Resource
    private AppProductMapper appProductMapper;
    @Resource
    private EsProductDocRepository esProductDocRepository;

    public int initEsData() {

        esProductDocRepository.deleteAll();

        List<AppProduct> appProductList = appProductMapper.selectList(
                new LambdaQueryWrapper<AppProduct>()
                        .eq(AppProduct::getIsOnSale, true)
        );

        List<EsProductDoc> esProductDocList = BeanUtils.toBeanList(appProductList, EsProductDoc.class);
        Iterable<EsProductDoc> iterable = esProductDocRepository.saveAll(esProductDocList);

        return ListUtil.toList(iterable).size();
    }
}
