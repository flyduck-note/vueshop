package com.flyduck.modules.admin.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AdminRefundQueryVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-13
 */
@Data
public class AdminRefundQueryReqVO implements Serializable {

    private String sn;
    private String orderSn;
    private String refundStatus;
}
