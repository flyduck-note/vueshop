package com.flyduck.mapper;

import com.flyduck.config.BaseMapperX;
import com.flyduck.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.flyduck.entity.SysRoleMenu
 */
public interface SysRoleMenuMapper extends BaseMapperX<SysRoleMenu> {

}




