package com.flyduck.modules.system.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.flyduck.entity.SysRole;
import com.flyduck.entity.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * SysRoleDetailRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysUserDetailsRespVO extends SysUser implements Serializable {

    @JsonIgnore
    private String password;

    private List<SysRoleDTO> roles;

    @Data
    public static class SysRoleDTO extends SysRole implements Serializable {

    }
}
