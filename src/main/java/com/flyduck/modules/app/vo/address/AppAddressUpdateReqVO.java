package com.flyduck.modules.app.vo.address;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * AppAddressSaveReqVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-19
 */
@Data
public class AppAddressUpdateReqVO implements Serializable {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    @NotBlank(message = "收货人名称不能为空")
    private String name;

    /**
     *
     */
    @NotBlank(message = "收货人电话不能为空")
    private String tel;

    /**
     *
     */
    @NotBlank(message = "收货地址不能为空")
    private String province;

    /**
     *
     */
    @NotBlank(message = "收货地址不能为空")
    private String city;

    /**
     *
     */
    @NotBlank(message = "收货地址不能为空")
    private String county;

    /**
     *
     */
    @NotBlank(message = "收货地址不能为空")
    private String addressDetail;

    /**
     *
     */
    private String areaCode;

    /**
     * 邮政编码
     */
    private String postalCode;

    /**
     *
     */
    @NotNull(message = "是否默认地址不能为空")
    private Boolean isDefault;
}
