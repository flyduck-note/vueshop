package com.flyduck.mapper;

import com.flyduck.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity com.flyduck.entity.SysUser
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> getUserListByRoleId(@Param("roleId") Long roleId);

    List<SysUser> getUserListByMenuId(@Param("menuId") Long menuId);
}




