package com.flyduck.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.entity.AppComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flyduck.modules.app.vo.comment.AppCommentRespVO;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity com.flyduck.entity.AppComment
 */
public interface AppCommentMapper extends BaseMapper<AppComment> {

    Page<AppCommentRespVO> getCommentPageByProductId(@Param("page") Page page, @Param("productId") Long productId);
}




