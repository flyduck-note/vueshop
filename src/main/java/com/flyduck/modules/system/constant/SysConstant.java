package com.flyduck.modules.system.constant;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-02 09:25
 **/
public class SysConstant {
    public static final String TOKEN_HEADER = "Authorization";
    public static final String DEFAULT_PASSWORD = "888888";
    public static final String DEFAULT_AVATAR = "https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/5a9f48118166308daba8b6da7e466aab.jpg";

    public static enum CommonStatus  {
        ENABLE(0, "启用"),
        DISABLE(1, "禁用");

        private int code;
        private String description;

        CommonStatus (int code, String description) {
            this.code = code;
            this.description = description;
        }

        public int getCode() {
            return code;
        }
    }

    public static enum MenuType{
        DIRECTORY(0, "目录"),
        MENU(1, "菜单"),
        BUTTON(2, "按钮");

        private int code;
        private String description;

        MenuType(int code, String description) {
            this.code = code;
            this.description = description;
        }

        public int getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }
    }
}
