package com.flyduck.utils;

import cn.hutool.core.date.DateUtil;

import java.util.Date;

/**
 * <p>
 * DateUtils
 * </p>
 *
 * @author flyduck
 * @since 2024/5/8
 */
public class DateUtils extends DateUtil {

    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String YYYYMMDD = "yyyyMMdd";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public static final String YYYYMMDDHHMM = "yyyyMMddHHmm";


    public static String getYYYY_MM_DD(){
        return format(new Date(), YYYY_MM_DD);
    }
}
