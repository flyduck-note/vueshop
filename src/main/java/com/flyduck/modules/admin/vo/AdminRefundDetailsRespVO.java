package com.flyduck.modules.admin.vo;

import com.flyduck.entity.AppOrder;
import com.flyduck.entity.AppOrderItem;
import com.flyduck.entity.AppRefund;
import com.flyduck.modules.app.constant.AppConstant;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * AdminRefundDetailsRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-13
 */
@Data
public class AdminRefundDetailsRespVO implements Serializable {

    private AppRefundVO refund;
    private AppOrderVO order;



    @Data
    public static class AppRefundVO extends AppRefund implements Serializable{
        private String refundStatusStr;

        public String getRefundStatusStr() {
            return AppConstant.RefundStatus.fromCode(getRefundStatus()).getDescription();
        }
    }

    @Data
    public static class AppOrderVO extends AppOrder implements Serializable{
        private List<AppOrderItemVO> orderItems;
    }

    @Data
    public static class AppOrderItemVO extends AppOrderItem implements Serializable{
    }
}
