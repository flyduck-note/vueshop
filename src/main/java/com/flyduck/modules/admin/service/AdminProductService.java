package com.flyduck.modules.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.modules.admin.vo.*;

import java.util.List;

/**
 * <p>
 * AdminProductService
 * </p>
 *
 * @author flyduck
 * @since 2024-06-05
 */
public interface AdminProductService {
    Page<AdminProductRespVO> getProductPage(Page page, AdminProductQueryReqVO adminProductQueryReqVO);

    AdminProductDetailsRespVO getProductDetailsById(Long id);

    void saveProduct(AdminProductSaveReqVO adminProductSaveReqVO);

    void updateProduct(AdminProductUpdateReqVO adminProductUpdateReqVO);

    void batchDeleteByIds(List<Long> ids);

    void updateStatusById(Long id, AdminProductUpdateStatusReqVO adminProductUpdateStatusReqVO);
}
