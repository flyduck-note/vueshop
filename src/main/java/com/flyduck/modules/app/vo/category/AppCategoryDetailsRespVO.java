package com.flyduck.modules.app.vo.category;

import com.flyduck.entity.AppCategory;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-16 10:15
 **/
@Data
public class AppCategoryDetailsRespVO extends AppCategory implements Serializable {
}
