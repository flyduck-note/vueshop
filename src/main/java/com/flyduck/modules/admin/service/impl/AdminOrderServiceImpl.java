package com.flyduck.modules.admin.service.impl;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.exception.BusinessException;
import com.flyduck.entity.AppOrder;
import com.flyduck.entity.AppOrderItem;
import com.flyduck.entity.AppRefund;
import com.flyduck.entity.AppUser;
import com.flyduck.manager.AppOrderManager;
import com.flyduck.mapper.AppOrderItemMapper;
import com.flyduck.mapper.AppOrderMapper;
import com.flyduck.mapper.AppRefundMapper;
import com.flyduck.mapper.AppUserMapper;
import com.flyduck.modules.admin.service.AdminOrderService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.modules.app.constant.AppConstant;
import com.flyduck.modules.app.vo.delivery.AppDeliveryInfoRespVO;
import com.flyduck.dto.SysUserInfoDTO;
import com.flyduck.utils.BeanUtils;
import com.flyduck.utils.KdApiSearchUtils;
import com.flyduck.utils.SecurityUtils;
import com.flyduck.utils.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * AdminOrderServiceImpl
 * </p>
 *
 * @author flyduck
 * @since 2024-06-11
 */
@Service
public class AdminOrderServiceImpl implements AdminOrderService {

    @Resource
    private AppOrderMapper appOrderMapper;
    @Resource
    private AppOrderItemMapper appOrderItemMapper;
    @Resource
    private AppUserMapper appUserMapper;
    @Resource
    private AppRefundMapper appRefundMapper;
    @Resource
    private AppOrderManager appOrderManager;

    @Override
    public Page<AdminOrderRespVO> getOrderPage(Page page, AdminOrderQueryReqVO adminOrderQueryReqVO) {
        return appOrderMapper.getOrderPage(page, adminOrderQueryReqVO);
    }

    @Override
    public List<AdminDictRespVO> getOrderStatusList() {
        List<AdminDictRespVO> adminDictRespVOList = new ArrayList<>();
        for (AppConstant.OrderStatus orderStatus : AppConstant.OrderStatus.values()) {
            AdminDictRespVO adminDictRespVO = new AdminDictRespVO();
            adminDictRespVO.setKey(orderStatus.getCode());
            adminDictRespVO.setValue(orderStatus.getDescription());
            adminDictRespVOList.add(adminDictRespVO);
        }
        return adminDictRespVOList;
    }

    @Override
    public AdminOrderDetailsRespVO getOrderDetailsById(Long id) {
        //订单基本信息
        AppOrder appOrder = appOrderMapper.selectById(id);
        if(appOrder == null){
            throw new BusinessException("该订单不存在");
        }
        AdminOrderDetailsRespVO adminOrderDetailsRespVO = BeanUtils.toBean(appOrder, AdminOrderDetailsRespVO.class);

        //订单项列表
        List<AppOrderItem> appOrderItemList = appOrderItemMapper.getAppOrderItemListByOrderId(id);
        List<AdminOrderDetailsRespVO.AppOrderItemVO> appOrderItemVOList = BeanUtils.toBeanList(appOrderItemList, AdminOrderDetailsRespVO.AppOrderItemVO.class);
        adminOrderDetailsRespVO.setOrderItems(appOrderItemVOList);

        //订单用户信息
        AppUser appUser = appUserMapper.selectById(appOrder.getUserId());
        adminOrderDetailsRespVO.setUsername(appUser.getUsername());
        adminOrderDetailsRespVO.setUserAvatar(appUser.getAvatar());

        //退款信息
        AppRefund appRefund = appRefundMapper.selectOne(
                new LambdaQueryWrapper<AppRefund>()
                        .eq(AppRefund::getOrderId, id)
        );
        AdminOrderDetailsRespVO.AppRefundVO appRefundVO = BeanUtils.toBean(appRefund, AdminOrderDetailsRespVO.AppRefundVO.class);
        adminOrderDetailsRespVO.setAppRefund(appRefundVO);
        return adminOrderDetailsRespVO;
    }

    @Override
    public void orderDelivery(AdminOrderDeliveryReqVO adminOrderDeliveryReqVO) {
        AppOrder appOrder = appOrderMapper.selectById(adminOrderDeliveryReqVO.getId());
        if (appOrder == null) {
            throw new BusinessException("该订单不存在");
        }

        if(AppConstant.OrderStatus.PENDING_DELIVERY.getCode() != appOrder.getOrderStatus()){
            throw new BusinessException("该订单状态不允许发货");
        }

        if(StringUtils.isBlank(adminOrderDeliveryReqVO.getDeliveryCompany()) || StringUtils.isBlank(adminOrderDeliveryReqVO.getDeliverySn())){
            throw new BusinessException("参数异常");
        }

        appOrder.setDeliveryCompany(adminOrderDeliveryReqVO.getDeliveryCompany());
        appOrder.setDeliverySn(adminOrderDeliveryReqVO.getDeliverySn());
        appOrder.setDeliveryTime(LocalDateTime.now());
        appOrder.setOrderStatus(AppConstant.OrderStatus.PENDING_RECEIPT.getCode());
        appOrder.setUpdated(LocalDateTime.now());

        //记录发货人
        SysUserInfoDTO userInfo = SecurityUtils.getUserInfo();
        appOrder.setAdminNote("管理员：" + userInfo.getId() + "发货：" + appOrder.getAdminNote());

        appOrderMapper.updateById(appOrder);
    }

    @Override
    public List<AdminDeliveryTraceRespVO> getDeliveryTraceListById(Long id) throws Exception {
        AppOrder appOrder = appOrderMapper.selectById(id);

        String deliveryInfo = new KdApiSearchUtils().getDeliveryInfo(appOrder.getDeliveryCompany(), appOrder.getDeliverySn());

        JSONArray traces = JSONUtil.parseObj(deliveryInfo).getJSONArray("Traces");
        return traces.stream()
                .map(trace -> {
                    JSONObject jsonObject = JSONUtil.parseObj(trace);
                    AdminDeliveryTraceRespVO adminDeliveryTraceRespVO = new AdminDeliveryTraceRespVO();
                    adminDeliveryTraceRespVO.setAcceptTime(jsonObject.getStr("AcceptTime"));
                    adminDeliveryTraceRespVO.setAcceptStation(jsonObject.getStr("AcceptStation"));
                    adminDeliveryTraceRespVO.setRemark(jsonObject.getStr("Remark"));

                    return adminDeliveryTraceRespVO;
                }).collect(Collectors.toList());
    }

    @Override
    public void closeOrder(AdminOrderCloseReqVO adminOrderDeliveryReqVO) {
        SysUserInfoDTO userInfo = SecurityUtils.getUserInfo();
        appOrderManager.closeOrder(adminOrderDeliveryReqVO.getId(),adminOrderDeliveryReqVO.getAdminNote(),userInfo.getId());
    }
}
