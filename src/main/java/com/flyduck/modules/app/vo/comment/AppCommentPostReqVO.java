package com.flyduck.modules.app.vo.comment;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * AppPostCommentReqVO
 * </p>
 *
 * @author flyduck
 * @since 2024-05-24
 */
@Data
public class AppCommentPostReqVO implements Serializable {
    @NotNull(message = "评论商品id不能为空")
    private Long orderItemId;

    @NotBlank(message = "评论内容不能为空")
    private String content;

    @NotNull(message = "评论分数不能为空")
    private Integer score;
}
