package com.flyduck.modules.system.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * SysRoleQueryReqVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysUserQueryReqVO implements Serializable {

    private String username;
}
