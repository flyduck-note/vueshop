package com.flyduck.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.config.BaseMapperX;
import com.flyduck.entity.AppProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flyduck.modules.admin.vo.AdminProductQueryReqVO;
import com.flyduck.modules.admin.vo.AdminProductRespVO;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity com.flyduck.entity.AppProduct
 */
public interface AppProductMapper extends BaseMapperX<AppProduct> {

    int reduceStock(@Param("productId") Long productId, @Param("quantity") Integer quantity);

    int releaseStock(@Param("productId") Long productId, @Param("quantity") Integer quantity);

    Page<AdminProductRespVO> getProductPage(@Param("page") Page page, @Param("adminProductQueryReqVO") AdminProductQueryReqVO adminProductQueryReqVO);
}




