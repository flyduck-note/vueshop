package com.flyduck.modules.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.admin.service.AdminProductService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.modules.app.controller.BaseController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * <p>
 * SysMenuController
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@RestController
@RequestMapping("/admin/product")
public class AdminProductController extends BaseController {

    @Resource
    private AdminProductService adminProductService;

    @GetMapping("/getProductPage")
    public Result<Page<AdminProductRespVO>> getProductPage(AdminProductQueryReqVO adminProductQueryReqVO){
        Page<AdminProductRespVO> adminProductRespVOPage = adminProductService.getProductPage(getPage(),adminProductQueryReqVO);
        return Result.success(adminProductRespVOPage);
    }

    @GetMapping("/getProductDetailsById/{id}")
    public Result<AdminProductDetailsRespVO> getProductDetailsById(@PathVariable("id") Long id){
        AdminProductDetailsRespVO adminProductDetailsRespVO = adminProductService.getProductDetailsById(id);
        return Result.success(adminProductDetailsRespVO);
    }

    @PostMapping("/saveProduct")
    public Result<Void> saveProduct(@Validated @RequestBody AdminProductSaveReqVO adminProductSaveReqVO){
        adminProductService.saveProduct(adminProductSaveReqVO);
        return Result.success();
    }

    @PostMapping("/updateProduct")
    public Result<Void> updateProduct(@Validated @RequestBody AdminProductUpdateReqVO adminProductUpdateReqVO){
        adminProductService.updateProduct(adminProductUpdateReqVO);
        return Result.success();
    }

    @PostMapping("/batchDeleteByIds")
    public Result<Void> batchDeleteByIds(@RequestBody Long[] ids){
        adminProductService.batchDeleteByIds(Arrays.asList(ids));
        return Result.success();
    }

    @PostMapping("/updateStatusById/{id}")
    public Result<Void> updateStatusById(@PathVariable("id") Long id,AdminProductUpdateStatusReqVO adminProductUpdateStatusReqVO){
        adminProductService.updateStatusById(id,adminProductUpdateStatusReqVO);
        return Result.success();
    }


}
