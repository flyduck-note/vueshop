package com.flyduck.modules.system.vo;

import com.flyduck.entity.SysMenu;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * SysMenuTreeRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysMenuTreeRespVO extends SysMenu implements Serializable {
    private List<SysMenuTreeRespVO> children = new ArrayList<>();
}
