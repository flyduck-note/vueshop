package com.flyduck.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.entity.AppRefund;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flyduck.modules.admin.vo.AdminRefundQueryReqVO;
import com.flyduck.modules.admin.vo.AdminRefundRespVO;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity com.flyduck.entity.AppRefund
 */
public interface AppRefundMapper extends BaseMapper<AppRefund> {

    Page<AdminRefundRespVO> getRefundPage(@Param("page") Page page, @Param("adminRefundQueryReqVO") AdminRefundQueryReqVO adminRefundQueryReqVO);
}




