package com.flyduck.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flyduck.entity.AppAddress;
import com.flyduck.mapper.AppAddressMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-20 20:57
 **/
@Component
public class AppAddressManager {
    @Resource
    private AppAddressMapper appAddressMapper;

    /**
     * 根据用户id获取用户的默认地址信息
     * @param userId
     * @return
     */
    public AppAddress getDefaultAddress(Long userId) {
        return appAddressMapper.selectOne(
                new LambdaQueryWrapper<AppAddress>()
                .eq(AppAddress::getUserId,userId)
                .eq(AppAddress::getIsDefault,true)
        );
    }
}
