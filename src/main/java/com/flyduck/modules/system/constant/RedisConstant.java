package com.flyduck.modules.system.constant;

/**
 * <p>
 * RedisKeyPrefixConstant
 * </p>
 *
 * @author flyduck
 * @since 2024-05-28
 */
public class RedisConstant {

    public static final String LOGIN_CAPTCHA_KEY = "login:captcha";
    public static final Integer LOGIN_CAPTCHA_EXPIRE = 120;

    public static final String USER_PERMISSIONS_KEY = "user:permissions:";
    public static final Integer USER_PERMISSIONS_EXPIRE = 60 * 60;//一小时
}
