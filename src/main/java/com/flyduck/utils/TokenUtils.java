package com.flyduck.utils;

import com.flyduck.modules.app.constant.AppConstant;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-18 18:38
 **/
@Component
public class TokenUtils {

    @Resource
    private HttpSession httpSession;

    /**
     * 获取当前登录用户的id
     * @return
     */
    public Long getCurrentUserId(){
        Long userId = (Long) httpSession.getAttribute(AppConstant.SESSION_USER_KEY);
        return userId != null ? userId : -1;
    }


}
