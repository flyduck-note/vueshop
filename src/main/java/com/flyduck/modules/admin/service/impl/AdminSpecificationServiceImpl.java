package com.flyduck.modules.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.entity.AppCategory;
import com.flyduck.entity.AppSpecification;
import com.flyduck.mapper.AppCategoryMapper;
import com.flyduck.mapper.AppSpecificationMapper;
import com.flyduck.modules.admin.service.AdminSpecificationService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.utils.BeanUtils;
import com.flyduck.utils.PageUtils;
import com.flyduck.utils.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * AdminSpecificationServiceImpl
 * </p>
 *
 * @author flyduck
 * @since 2024-06-05
 */
@Service
public class AdminSpecificationServiceImpl implements AdminSpecificationService {

    @Resource
    private AppSpecificationMapper appSpecificationMapper;

    @Override
    public Page<AdminSpecificationRespVO> getSpecificationPage(Page page, AdminSpecificationQueryReqVO adminSpecificationQueryReqVO) {
        Page<AppSpecification> appSpecificationPage = appSpecificationMapper.selectPage(page,
                new LambdaQueryWrapper<AppSpecification>()
                        .like(StringUtils.isNotBlank(adminSpecificationQueryReqVO.getName()), AppSpecification::getName, adminSpecificationQueryReqVO.getName())
                        .orderByDesc(AppSpecification::getSortOrder,AppSpecification::getCreated)
        );

        return PageUtils.convert(appSpecificationPage, AdminSpecificationRespVO.class);
    }

    @Override
    public AdminSpecificationDetailsRespVO getSpecificationDetailsById(Long id) {
        AppSpecification appSpecification = appSpecificationMapper.selectById(id);
        AdminSpecificationDetailsRespVO adminSpecificationDetailsRespVO = BeanUtils.toBean(appSpecification, AdminSpecificationDetailsRespVO.class);
        if(StringUtils.isBlank(adminSpecificationDetailsRespVO.getOptions())){
            adminSpecificationDetailsRespVO.setOptions("");
        }
        return adminSpecificationDetailsRespVO;
    }

    @Override
    public void saveSpecification(AdminSpecificationSaveReqVO adminSpecificationSaveReqVO) {
        AppSpecification appSpecification = BeanUtils.toBean(adminSpecificationSaveReqVO, AppSpecification.class);
        appSpecification.setCreated(LocalDateTime.now());
        appSpecificationMapper.insert(appSpecification);
    }

    @Override
    public void updateSpecification(AdminSpecificationUpdateReqVO adminSpecificationUpdateReqVO) {
        AppSpecification appSpecification = BeanUtils.toBean(adminSpecificationUpdateReqVO, AppSpecification.class);
        appSpecification.setUpdated(LocalDateTime.now());
        appSpecificationMapper.updateById(appSpecification);
    }

    @Override
    public void batchDeleteByIds(List<Long> ids) {
        appSpecificationMapper.deleteBatchIds(ids);
    }
}
