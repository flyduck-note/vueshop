package com.flyduck.modules.app.vo.refund;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-23 21:08
 **/
@Data
public class AppRefundApplyReqVO implements Serializable {

    @NotNull(message = "订单不能为空")
    private Long orderId;

    /**
     * 退货方式，0-自行寄回，1-上门取货
     */
    @NotBlank(message = "退货方式不能为空")
    private String method;

    /**
     *
     */
    private BigDecimal refundAmount;

    /**
     * 退款理由
     */
    @NotBlank(message = "退款理由不能为空")
    private String reason;

    private String description;

    private String images;

    private Long operatorId;

    private String operateRemark;

    private LocalDateTime operateTime;

    private String receiptRemark;
}
