package com.flyduck.modules.system.vo;

import com.flyduck.entity.SysRole;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * SysRoleDetailRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysRoleDetailsRespVO extends SysRole implements Serializable {

    private List<Long> menuIds = new ArrayList<>();
}
