package com.flyduck.modules.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.exception.BusinessException;
import com.flyduck.entity.AppAd;
import com.flyduck.entity.AppCategory;
import com.flyduck.entity.AppProduct;
import com.flyduck.mapper.AppCategoryMapper;
import com.flyduck.mapper.AppProductMapper;
import com.flyduck.modules.admin.service.AdminCategoryService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.utils.BeanUtils;
import com.flyduck.utils.PageUtils;
import com.flyduck.utils.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * AdminCategoryServiceImpl
 * </p>
 *
 * @author flyduck
 * @since 2024-06-04
 */
@Service
public class AdminCategoryServiceImpl implements AdminCategoryService {

    @Resource
    private AppCategoryMapper appCategoryMapper;
    @Resource
    private AppProductMapper appProductMapper;

    @Override
    public Page<AdminCategoryRespVO> getCategoryPage(Page page, AdminCategoryQueryReqVO sysCategoryQueryReqVO) {
        Page<AppCategory> appCategoryPage = appCategoryMapper.selectPage(page,
                new LambdaQueryWrapper<AppCategory>()
                        .like(StringUtils.isNotBlank(sysCategoryQueryReqVO.getName()), AppCategory::getName, sysCategoryQueryReqVO.getName())
                        .orderByDesc(AppCategory::getCreated)
        );

        return PageUtils.convert(appCategoryPage, AdminCategoryRespVO.class);
    }

    @Override
    public AdminCategoryDetailsRespVO getCategoryDetailsById(Long id) {
        AppCategory appCategory = appCategoryMapper.selectById(id);
        return BeanUtils.toBean(appCategory,AdminCategoryDetailsRespVO.class);
    }

    @Override
    public void saveCategory(AdminCategorySaveReqVO adminCategorySaveReqVO) {
        AppCategory appCategory = BeanUtils.toBean(adminCategorySaveReqVO, AppCategory.class);
        appCategory.setCreated(LocalDateTime.now());
        appCategoryMapper.insert(appCategory);
    }

    @Override
    public void updateCategory(AdminCategoryUpdateReqVO adminCategoryUpdateReqVO) {
        AppCategory appCategory = BeanUtils.toBean(adminCategoryUpdateReqVO, AppCategory.class);
        appCategory.setUpdated(LocalDateTime.now());
        appCategoryMapper.updateById(appCategory);
    }

    @Override
    public void batchDeleteByIds(List<Long> ids) {
        // 如果分类关联了产品，就不能删除
        Long count = appProductMapper.selectCount(
                new LambdaQueryWrapper<AppProduct>()
                        .in(AppProduct::getCategoryId, ids)
        );
        if(count > 0){
            throw new BusinessException("该分类下有管理商品，不允许删除");
        }
        appCategoryMapper.deleteBatchIds(ids);
    }

    @Override
    public List<AdminCategoryRespVO> getCategoryList() {
        List<AppCategory> appCategoryList = appCategoryMapper.selectList(
                new LambdaQueryWrapper<AppCategory>()
        );
        return BeanUtils.toBeanList(appCategoryList,AdminCategoryRespVO.class);
    }
}
