package com.flyduck.modules.system.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * SysRoleQueryReqVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysRoleQueryReqVO implements Serializable {

    private String name;
}
