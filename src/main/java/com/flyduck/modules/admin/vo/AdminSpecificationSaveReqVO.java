package com.flyduck.modules.admin.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-06-03 21:48
 **/
@Data
public class AdminSpecificationSaveReqVO implements Serializable {

    /**
     *
     */
    @NotBlank(message = "规格名称不能为空")
    private String name;

    /**
     * 可选项
     */
    @NotBlank(message = "可选值不能为空")
    private String options;

    /**
     *
     */
    @NotNull(message = "排序不能为空")
    private Integer sortOrder;
}
