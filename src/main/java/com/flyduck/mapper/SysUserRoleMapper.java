package com.flyduck.mapper;

import com.flyduck.config.BaseMapperX;
import com.flyduck.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.flyduck.entity.SysUserRole
 */
public interface SysUserRoleMapper extends BaseMapperX<SysUserRole> {

}




