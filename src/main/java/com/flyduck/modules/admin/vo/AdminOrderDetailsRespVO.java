package com.flyduck.modules.admin.vo;

import com.flyduck.entity.AppOrder;
import com.flyduck.entity.AppOrderItem;
import com.flyduck.entity.AppRefund;
import com.flyduck.modules.app.constant.AppConstant;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * AdminOrderDetailsRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-11
 */
@Data
public class AdminOrderDetailsRespVO extends AppOrder implements Serializable {

    private List<AppOrderItemVO> orderItems;
    private AppRefundVO appRefund;

    private String username;
    private String userAvatar;

    //订单状态中文
    private String orderStatusStr;

    public String getOrderStatusStr() {
        return AppConstant.OrderStatus.fromCode(getOrderStatus()).getDescription();
    }

    public static class AppOrderItemVO extends AppOrderItem implements Serializable{

    }

    public static class AppRefundVO extends AppRefund implements Serializable{

    }
}
