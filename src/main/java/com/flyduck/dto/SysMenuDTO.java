package com.flyduck.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * SysMenuDTO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-03
 */
@Data
public class SysMenuDTO implements Serializable {

    private Long id;
    private Long parentId;
    private Integer type;

    private String title;
    private String icon;
    private String path;
    private String component;
    private String name;

    private List<SysMenuDTO> children = new ArrayList<>();
}
