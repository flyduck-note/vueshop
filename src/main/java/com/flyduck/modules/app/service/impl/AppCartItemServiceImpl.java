package com.flyduck.modules.app.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.flyduck.common.exception.BusinessException;
import com.flyduck.entity.AppCartItem;
import com.flyduck.entity.AppProduct;
import com.flyduck.entity.AppSkuStock;
import com.flyduck.mapper.AppCartItemMapper;
import com.flyduck.mapper.AppProductMapper;
import com.flyduck.modules.app.vo.cartItem.AppCartItemAddReqVO;
import com.flyduck.modules.app.vo.cartItem.AppCartItemRespVO;
import com.flyduck.manager.AppSkuStockManager;
import com.flyduck.utils.TokenUtils;
import com.flyduck.modules.app.service.AppCartItemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: vueshop
 * @description:
 * @author: flyduck
 * @create: 2024-05-19 09:30
 **/
@Service
public class AppCartItemServiceImpl implements AppCartItemService {

    @Resource
    private TokenUtils tokenUtils;
    @Resource
    private AppCartItemMapper appCartItemMapper;
    @Resource
    private AppSkuStockManager appSkuStockManager;
    @Resource
    private AppProductMapper appProductMapper;

    @Override
    public List<AppCartItemRespVO> getCartItemList() {
        Long userId = tokenUtils.getCurrentUserId();
        List<AppCartItemRespVO> appCartItemRespVOList =appCartItemMapper.getCartItemListByUserId(userId);
        return appCartItemRespVOList;
    }

    @Override
    public Long getTotalCount() {
        Long userId = tokenUtils.getCurrentUserId();
        Long totalCount = appCartItemMapper.selectCount(
                new LambdaQueryWrapper<AppCartItem>()
                        .eq(AppCartItem::getUserId, userId)
        );
        return totalCount;
    }

    @Override
    public BigDecimal getTotalAmountByIds(ArrayList<Long> ids) {
        Long userId = tokenUtils.getCurrentUserId();
        if(CollectionUtil.isEmpty(ids)){
            return BigDecimal.ZERO;
        }
        //获取选中的购物车
        return appCartItemMapper.getTotalAmountByIdsAndUserId(ids,userId);
    }

    @Override
    public void updateQuantityById(Long cartItemId, Integer quantity) {
        Long userId = tokenUtils.getCurrentUserId();
        AppCartItem appCartItem = appCartItemMapper.selectById(cartItemId);

        //判断库存
        AppSkuStock appSkuStock = appSkuStockManager.getSkuStock(appCartItem.getSkuId(),appCartItem.getProductId());
        if(quantity > appSkuStock.getStock()){
            throw new BusinessException("库存不足");
        }

        //更新数量
        appCartItemMapper.update(
                null,
                new LambdaUpdateWrapper<AppCartItem>()
                .eq(AppCartItem::getId,cartItemId)
                .eq(AppCartItem::getUserId,userId)
                .set(AppCartItem::getQuantity, quantity)
                .set(AppCartItem::getUpdated, LocalDateTime.now())
        );
    }

    @Override
    public void addCartItem(AppCartItemAddReqVO appCartItemAddReqVO) {
        Long userId = tokenUtils.getCurrentUserId();
        //判断商品是否为空、是否上架
        AppProduct appProduct = appProductMapper.selectById(appCartItemAddReqVO.getProductId());
        if (appProduct == null || !appProduct.getIsOnSale()) {
            throw new BusinessException("商品未上架");
        }

        // 判断库存是否足够
        AppSkuStock appSkuStock = appSkuStockManager.getSkuStock(appCartItemAddReqVO.getSkuId(), appCartItemAddReqVO.getProductId());
        if(appCartItemAddReqVO.getQuantity() > appSkuStock.getStock()){
            throw new BusinessException("库存不足");
        }

        AppCartItem existCartItem = appCartItemMapper.selectOne(
                new LambdaQueryWrapper<AppCartItem>()
                        .eq(AppCartItem::getUserId, userId)
                        .eq(AppCartItem::getProductId, appCartItemAddReqVO.getProductId())
                        .eq(AppCartItem::getSkuId, appCartItemAddReqVO.getSkuId())
        );

        if (existCartItem != null) {
            // 购物车已经存在该商品的情况：原来数量 + 新数量
            existCartItem.setQuantity(existCartItem.getQuantity() + appCartItemAddReqVO.getQuantity());
            existCartItem.setUpdated(LocalDateTime.now());
            appCartItemMapper.updateById(existCartItem);
        }else {
            // 购物车未存在该商品的情况
            AppCartItem saveCartItem = new AppCartItem();
            saveCartItem.setUserId(userId);
            saveCartItem.setSkuId(appCartItemAddReqVO.getSkuId());
            saveCartItem.setProductId(appCartItemAddReqVO.getProductId());
            saveCartItem.setQuantity(appCartItemAddReqVO.getQuantity());
            saveCartItem.setCreated(LocalDateTime.now());
            appCartItemMapper.insert(saveCartItem);
        }
    }

    @Override
    public void batchDeleteByIds(Long[] ids) {
        Long userId = tokenUtils.getCurrentUserId();
        appCartItemMapper.delete(
                new LambdaQueryWrapper<AppCartItem>()
                .eq(AppCartItem::getUserId,userId)
                .in(AppCartItem::getId, ListUtil.toList(ids))
        );
    }
}
