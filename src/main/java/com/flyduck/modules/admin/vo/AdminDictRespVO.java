package com.flyduck.modules.admin.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * AdminDictRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-11
 */
@Data
public class AdminDictRespVO implements Serializable {

    private Integer key;
    private String value;
}
