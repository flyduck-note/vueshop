package com.flyduck.modules.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flyduck.common.lang.Result;
import com.flyduck.modules.admin.service.AdminOrderService;
import com.flyduck.modules.admin.vo.*;
import com.flyduck.modules.app.controller.BaseController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * AdminOrderController
 * </p>
 *
 * @author flyduck
 * @since 2024-06-11
 */
@RestController
@RequestMapping("/admin/order")
public class AdminOrderController extends BaseController {

    @Resource
    private AdminOrderService adminOrderService;

    @GetMapping("/getOrderPage")
    public Result<Page<AdminOrderRespVO>> getOrderPage(AdminOrderQueryReqVO adminOrderQueryReqVO){
        Page<AdminOrderRespVO> adminOrderRespVOPage = adminOrderService.getOrderPage(getPage(), adminOrderQueryReqVO);
        return Result.success(adminOrderRespVOPage);
    }

    @GetMapping("/getOrderStatusList")
    public Result<List<AdminDictRespVO>> getOrderStatusList(){
        List<AdminDictRespVO> adminDictRespVOList = adminOrderService.getOrderStatusList();
        return Result.success(adminDictRespVOList);
    }

    @GetMapping("/getOrderDetailsById/{id}")
    public Result<AdminOrderDetailsRespVO> getOrderDetailsById(@PathVariable("id") Long id){
        AdminOrderDetailsRespVO adminOrderDetailsRespVO = adminOrderService.getOrderDetailsById(id);
        return Result.success(adminOrderDetailsRespVO);
    }

    @PostMapping("/orderDelivery")
    public Result<Void> orderDelivery(@RequestBody AdminOrderDeliveryReqVO adminOrderDeliveryReqVO){
        adminOrderService.orderDelivery(adminOrderDeliveryReqVO);
        return Result.success();
    }

    @GetMapping("/getDeliveryTraceListById/{id}")
    public Result<List<AdminDeliveryTraceRespVO>> getDeliveryTraceListById(@PathVariable("id") Long id) throws Exception {
        List<AdminDeliveryTraceRespVO> deliveryTraceList = adminOrderService.getDeliveryTraceListById(id);
        return Result.success(deliveryTraceList);
    }

    @PostMapping("/closeOrder")
    public Result<Void> closeOrder(@RequestBody AdminOrderCloseReqVO adminOrderDeliveryReqVO){
        adminOrderService.closeOrder(adminOrderDeliveryReqVO);
        return Result.success();
    }

}
