package com.flyduck.modules.admin.service.impl;

import com.flyduck.manager.AppUploadManager;
import com.flyduck.modules.admin.service.AdminUploadService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * AdminUploadServiceImpl
 * </p>
 *
 * @author flyduck
 * @since 2024-06-27
 */
@Service
public class AdminUploadServiceImpl implements AdminUploadService {

    @Resource
    private AppUploadManager appUploadManager;

    @Override
    public List<String> upload(MultipartFile[] pics) throws IOException {
        return appUploadManager.upload(pics, 0.5F, 0.2);
    }
}
