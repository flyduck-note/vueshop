package com.flyduck.modules.app.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p>
 * AlipayProperties
 * </p>
 *
 * @author flyduck
 * @since 2024-05-22
 */
@Data
@Component
@ConfigurationProperties(prefix = "ali")
public class AlipayProperties {

    private String serverUrl;
    private String appId;
    private String privateKey;
    private String alipayPublicKey;
    private String notifyUrl;
    private String returnUrl;
}
