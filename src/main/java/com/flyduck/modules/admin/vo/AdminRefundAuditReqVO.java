package com.flyduck.modules.admin.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * AdminRefundAuditReqVO
 * </p>
 *
 * @author flyduck
 * @since 2024-06-13
 */
@Data
public class AdminRefundAuditReqVO implements Serializable {

    private Long id;

    /**
     * 申请状态：0->待处理；1->退货中；2->已完成；3->已拒绝
     */
    private Integer refundStatus;

    /**
     *
     */
    private BigDecimal refundAmount;


    /**
     *
     */
    private String operateRemark;

    private String receiptRemark;
}
