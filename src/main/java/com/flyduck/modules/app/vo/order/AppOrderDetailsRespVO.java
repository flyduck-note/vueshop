package com.flyduck.modules.app.vo.order;

import com.flyduck.entity.AppOrder;
import com.flyduck.entity.AppOrderItem;
import com.flyduck.entity.AppRefund;
import com.flyduck.modules.app.constant.AppConstant;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * AppOrderRespVO
 * </p>
 *
 * @author flyduck
 * @since 2024-05-22
 */
@Data
public class AppOrderDetailsRespVO extends AppOrder implements Serializable {

    private List<AppOrderItemVO> orderItems;

    private AppRefundVO appRefund;

    //订单状态中文
    private String orderStatusStr;

    public String getOrderStatusStr() {
        return AppConstant.OrderStatus.fromCode(getOrderStatus()).getDescription();
    }

    @Data
    public static class AppOrderItemVO extends AppOrderItem implements Serializable{

    }

    @Data
    public static class AppRefundVO extends AppRefund implements Serializable{

    }
}
